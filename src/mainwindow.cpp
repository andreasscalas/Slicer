#include "mainwindow.h"
#include "ui_mainwindow.h"


using namespace AndreasStructures;
#define VTK_CREATE(type, name) \
    vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow){
    ui->setupUi(this);
    this->init();
    this->write(initial_instruction);
    iren = this->ui->qvtkWidget->GetRenderWindow()->GetInteractor();
    connect(this->ui->actionLoad, SIGNAL(triggered()), this, SLOT(slotOpenFile()));
    connect(this->ui->actionClear, SIGNAL(triggered()), this, SLOT(slotClear()));
}

MainWindow::~MainWindow(){
    delete ui;
}

void MainWindow::init(){

    this->showSliceOnMesh = true;
    this->showBoundingBox = false;
    this->showSkeleton = false;
    this->showNodes = true;
    this->showArcNodes = false;
    this->showFittedCircle = false;
    this->showConvexHull = false;
    this->showBoundingRectangle = false;
    this->showDiagonals = false;
    this->showCenters = false;


    translationVector<< 0, 0, 0;

    model = new DrawableMesh();
    meshAssembly = vtkSmartPointer<vtkAssembly>::New();
    sliceAssembly = vtkSmartPointer<vtkAssembly>::New();
    ren = vtkSmartPointer<vtkRenderer>::New();
    ren->SetBackground(1.0, 1.0, 1.0);
    sliceRen = vtkSmartPointer<vtkRenderer>::New();
    sliceRen->SetBackground(1.0, 1.0, 1.0);
    this->allowedError = MAX_ERROR * this->ui->maxErrorSlider->value() / 100;
    this->ui->xSlider->setEnabled(true);
    this->ui->ySlider->setEnabled(true);
    this->ui->zSlider->setEnabled(true);
    this->ui->xSlider2->setEnabled(true);
    this->ui->ySlider2->setEnabled(true);
    this->ui->zSlider2->setEnabled(true);
    this->ui->maxErrorSlider->setEnabled(true);
    this->ui->qvtkWidget->GetRenderWindow()->AddRenderer(ren);
    this->ui->slideWidget->GetRenderWindow()->AddRenderer(sliceRen);

}

void MainWindow::clear(){

    boundingBox.clear();
    model = new DrawableMesh();
    this->ui->qvtkWidget->GetRenderWindow()->RemoveRenderer(ren);
    this->ui->slideWidget->GetRenderWindow()->RemoveRenderer(sliceRen);
    meshAssembly = vtkSmartPointer<vtkAssembly>::NewInstance(meshAssembly);
    sliceAssembly = vtkSmartPointer<vtkAssembly>::NewInstance(sliceAssembly);
    ren = vtkSmartPointer<vtkRenderer>::NewInstance(ren);
    ren->SetBackground(1.0, 1.0, 1.0);
    sliceRen = vtkSmartPointer<vtkRenderer>::NewInstance(sliceRen);
    sliceRen->SetBackground(1.0, 1.0, 1.0);
    this->ui->qvtkWidget->GetRenderWindow()->AddRenderer(ren);
    this->ui->slideWidget->GetRenderWindow()->AddRenderer(sliceRen);
    this->ui->qvtkWidget->GetRenderWindow()->ClearInRenderStatus();
    this->ui->slideWidget->GetRenderWindow()->ClearInRenderStatus();
    VTK_CREATE(vtkInteractorStyleImage, imageStyle);
    this->ui->slideWidget->GetRenderWindow()->GetInteractor()->SetInteractorStyle(imageStyle);
    this->ui->verticesLabel->setText("Vertices: ");
    this->ui->edgesLabel->setText("Edges: ");
    this->ui->trianglesLabel->setText("Triangles: ");
    this->ui->qvtkWidget->update();
    this->ui->slideWidget->update();
    this->ui->tabWidget->setEnabled(false);

}

void MainWindow::write(std::string message){
    VTK_CREATE(vtkVectorText, text);
    text->SetText(message.c_str());
    VTK_CREATE(vtkPolyDataMapper, mapper);
    mapper->ImmediateModeRenderingOn();
    mapper->SetInputConnection(text->GetOutputPort());
    VTK_CREATE(vtkActor, actor);
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(0, 0, 0);
    meshAssembly->AddPart(actor);
    ren->AddActor(meshAssembly);
    this->ui->qvtkWidget->update();
}

void MainWindow::updateView(){

    VTK_CREATE(vtkPlane, intersectingPlane);
    intersectingPlane->SetOrigin(center.x, center.y, center.z);
    intersectingPlane->SetNormal(normal.x, normal.y, normal.z);
    ren->Clear();
    ren->RemoveActor(meshAssembly);

    meshAssembly = vtkSmartPointer<vtkAssembly>::NewInstance(meshAssembly);
    model->draw(meshAssembly);
    sliceRen->RemoveActor(sliceAssembly);
    sliceAssembly = vtkSmartPointer<vtkAssembly>::NewInstance(sliceAssembly);
    VTK_CREATE(vtkCutter, cutter);
    cutter->SetCutFunction(intersectingPlane);
    VTK_CREATE(vtkPolyData, poly);
    poly->SetPoints(model->getPoints());
    poly->SetPolys(model->getTriangles());
    cutter->SetInputData(poly);
    cutter->Update();
    VTK_CREATE(vtkPolyData, pd);
    pd = cutter->GetOutput();
    VTK_CREATE(vtkPolyDataMapper, cutterMapper);
    cutterMapper->SetInputData(pd);

    VTK_CREATE(vtkActor, sliceActor);
    sliceActor->SetMapper(cutterMapper);
    sliceActor->GetProperty()->SetOpacity(1);
    sliceActor->GetProperty()->SetLineWidth(3);
    sliceActor->GetProperty()->SetColor(0, 0.5, 0);
    sliceActor->GetProperty()->SetRepresentationToWireframe();
    if(this->showSliceOnMesh)
        meshAssembly->AddPart(sliceActor);

    if(this->showBoundingBox){

        if(boundingBox.size() == 0){
            vector<double> bb = Utilities::getOBB(model);
            boundingBox.insert(boundingBox.end(), bb.begin(), bb.end());
        }

        VTK_CREATE(vtkPolyData, boundingBoxData);
        VTK_CREATE(vtkPoints, boundingBoxPoints);
        VTK_CREATE(vtkCellArray, boundingBoxLines);
        VTK_CREATE(vtkPolyDataMapper, boundingBoxMapper);
        VTK_CREATE(vtkActor, boundingBoxActor);
        VTK_CREATE(vtkOutlineSource, boundingBoxSource);

        boundingBoxSource->SetBoxTypeToOriented();
        boundingBoxSource->SetCorners(boundingBox.data());
        boundingBoxSource->Update();
        boundingBoxMapper->SetInputConnection(boundingBoxSource->GetOutputPort());
        boundingBoxActor->SetMapper(boundingBoxMapper);
        boundingBoxActor->GetProperty()->SetColor(1, 0, 0);
        meshAssembly->AddPart(boundingBoxActor);
    }

    vector<Slice*> slices = slicer->getSlices();

    Point slicesCenter(0, 0, 0);
    for(unsigned int i = 0; i < slices.size(); i++){

        DrawableMesh* sliceMesh = slices[i]->getSlice();
        slicesCenter += sliceMesh->getCenter();
        sliceMesh->draw(sliceAssembly);

        if(showFittedCircle){
            vector<pair<Point, double> > circles = slices[i]->getBestFittedCircles();

            for(unsigned int j = 0; j < circles.size(); j++){
                VTK_CREATE(vtkRegularPolygonSource, circleSource);
                circleSource->GeneratePolygonOff();
                circleSource->SetNumberOfSides(50);
                circleSource->SetNormal(0, 1, 0);
                circleSource->SetRadius(circles[j].second);
                circleSource->SetCenter(circles[j].first.x, circles[j].first.y, circles[j].first.z);
                VTK_CREATE(vtkPolyDataMapper, circleMapper);
                circleMapper->SetInputConnection(circleSource->GetOutputPort());
                VTK_CREATE(vtkActor, circleActor);
                circleActor->SetMapper(circleMapper);
                circleActor->GetProperty()->SetColor(BORDEAUX);
                circleActor->GetProperty()->SetLineWidth(3);
                sliceAssembly->AddPart(circleActor);
            }

        }

        if(showCenters){

            VTK_CREATE(vtkPolyData, tmpData);
            VTK_CREATE(vtkPolyData, centersData);
            VTK_CREATE(vtkPoints, centersPoints);
            VTK_CREATE(vtkVertexGlyphFilter, centersFilter);
            Point c = slices[i]->getSlice()->getCenter();
            if(showFittedCircle){
                vector<pair<Point, double> > circles = slices[i]->getBestFittedCircles();
                pair<Point, double> circle = circles[0];
                centersPoints->InsertNextPoint(circle.first.x, circle.first.y, circle.first.z);
            }
            centersPoints->InsertNextPoint(c.x, c.y, c.z);
            tmpData->SetPoints(centersPoints);
            centersFilter->SetInputData(tmpData);
            centersFilter->Update();
            centersData->ShallowCopy(centersFilter->GetOutput());
            VTK_CREATE(vtkDoubleArray, colors);/*
            colors->SetNumberOfComponents(2);
            colors->SetName("Colors");
            colors->InsertNextTuple(BORDEAUX);
            colors->InsertNextTuple(SLICE_COLOR);
            centersData->GetPointData()->SetScalars(colors);*/
            VTK_CREATE(vtkPolyDataMapper, centersMapper);
            centersMapper->SetInputData(centersData);
            VTK_CREATE(vtkActor, centersActor);
            centersActor->SetMapper(centersMapper);
            centersActor->GetProperty()->SetPointSize(3);
            centersActor->GetProperty()->SetColor(1, 0, 0);
            sliceAssembly->AddPart(centersActor);

        }

        if(showConvexHull){

            vector<Point> convexHull = slices[i]->getConvexHull();
            VTK_CREATE(vtkPolyData, chData);
            VTK_CREATE(vtkPoints, chPoints);
            VTK_CREATE(vtkCellArray, chLines);
            chPoints->InsertNextPoint(convexHull[0].x, convexHull[0].y, convexHull[0].z);

            for(int i = 1; i < convexHull.size(); i++){

                chPoints->InsertNextPoint(convexHull[i].x, convexHull[i].y, convexHull[i].z);
                VTK_CREATE(vtkLine, chLine);
                chLine->GetPointIds()->SetId(0, i - 1);
                chLine->GetPointIds()->SetId(1, i);
                chLines->InsertNextCell(chLine);

            }

            VTK_CREATE(vtkLine, chLine);
            chLine->GetPointIds()->SetId(0, convexHull.size() - 1);
            chLine->GetPointIds()->SetId(1, 0);
            chLines->InsertNextCell(chLine);
            chData->SetPoints(chPoints);
            chData->SetLines(chLines);

            VTK_CREATE(vtkPolyDataMapper, chMapper);
            chMapper->SetInputData(chData);
            VTK_CREATE(vtkActor, chActor);
            chActor->SetMapper(chMapper);
            chActor->GetProperty()->SetColor(1,0,1);
            sliceAssembly->AddPart(chActor);

        }

        if(showBoundingRectangle){

            VTK_CREATE(vtkPolyData, boundingRectangleData);
            VTK_CREATE(vtkPoints, boundingRectanglePoints);
            VTK_CREATE(vtkCellArray, boundingRectangleLines);

            vector<Point> bb = slices[i]->getBoundingBox();
            for(unsigned int i = 0; i < bb.size(); i++){
                cout<<"("<<bb[i].x<<", "<<bb[i].y<<", "<<bb[i].z<<")"<<endl<<flush;
                boundingRectanglePoints->InsertNextPoint(bb[i].x, bb[i].y, bb[i].z);
                VTK_CREATE(vtkLine, diagonal);
                diagonal->GetPointIds()->SetId(0, i );
                diagonal->GetPointIds()->SetId(1, (i + 1) % bb.size());
                boundingRectangleLines->InsertNextCell(diagonal);
            }

            boundingRectangleData->SetPoints(boundingRectanglePoints);
            boundingRectangleData->SetLines(boundingRectangleLines);

            VTK_CREATE(vtkPolyDataMapper, boundingRectangleMapper);
            boundingRectangleMapper->SetInputData(boundingRectangleData);
            VTK_CREATE(vtkActor, boundingRectangleActor);
            boundingRectangleActor->SetMapper(boundingRectangleMapper);
            boundingRectangleActor->GetProperty()->SetColor(1, 0, 1);
            sliceAssembly->AddPart(boundingRectangleActor);

        }

        if(showDiagonals){

            VTK_CREATE(vtkPolyData, diagonalsData);
            VTK_CREATE(vtkPoints, diagonalsPoints);
            VTK_CREATE(vtkCellArray, diagonalsLines);

            pair<Point, Point> minDiagonal = slices[i]->getMinDiagonal();
            pair<Point, Point> maxDiagonal = slices[i]->getMaxDiagonal();

            diagonalsPoints->InsertNextPoint(minDiagonal.first.x, minDiagonal.first.y, minDiagonal.first.z);
            diagonalsPoints->InsertNextPoint(minDiagonal.second.x, minDiagonal.second.y, minDiagonal.second.z);
            diagonalsPoints->InsertNextPoint(maxDiagonal.first.x, maxDiagonal.first.y, maxDiagonal.first.z);
            diagonalsPoints->InsertNextPoint(maxDiagonal.second.x, maxDiagonal.second.y, maxDiagonal.second.z);
            VTK_CREATE(vtkLine, diagonal1);
            diagonal1->GetPointIds()->SetId(0, 0);
            diagonal1->GetPointIds()->SetId(1, 1);
            diagonalsLines->InsertNextCell(diagonal1);
            VTK_CREATE(vtkLine, diagonal2);
            diagonal2->GetPointIds()->SetId(0, 2);
            diagonal2->GetPointIds()->SetId(1, 3);
            diagonalsLines->InsertNextCell(diagonal2);


            diagonalsData->SetPoints(diagonalsPoints);
            diagonalsData->SetLines(diagonalsLines);

            VTK_CREATE(vtkPolyDataMapper, diagonalsMapper);
            diagonalsMapper->SetInputData(diagonalsData);
            VTK_CREATE(vtkActor, diagonalsActor);
            diagonalsActor->SetMapper(diagonalsMapper);
            diagonalsActor->GetProperty()->SetColor(1,0,1);
            sliceAssembly->AddPart(diagonalsActor);

            double eccentricity = (maxDiagonal.second - maxDiagonal.first).length() /
                                  (minDiagonal.second - minDiagonal.first).length();
            cout<<"Eccentricity = "<<eccentricity<<flush<<endl;

        }

        if(showSkeleton){

            MANode* skeleton = slices[i]->getSkeleton();

            vector<pair<MANode*, MANode*> > arcs = skeleton->getGraphArcs();
            VTK_CREATE(vtkPolyData, skeletonData);
            VTK_CREATE(vtkPolyData, skeletonNodesData);

            VTK_CREATE(vtkPoints, skeletonPoints);
            VTK_CREATE(vtkPoints, skeletonNodesPoints);
            VTK_CREATE(vtkCellArray, skeletonLines);

            VTK_CREATE(vtkIdList, cellpoints);
            for(vector<pair<MANode*, MANode*> >::iterator ait = arcs.begin(); ait != arcs.end(); ait++){
                pair<MANode*, MANode*> p = (pair<MANode*, MANode*>) *ait;
                VTK_CREATE(vtkLine, line);
                Point* p1 = p.first->getPoint();
                Point* p2 = p.second->getPoint();
                int pid1, pid2;

                if(p1->info == nullptr){
                    pid1 = skeletonPoints->InsertNextPoint(p1->x, p1->y, p1->z);
                    cellpoints->InsertNextId(pid1);
                }else
                    pid1 = *static_cast<int*>(p1->info);

                vector<Point> commonPath = p.first->getCommonPath(p.second);

                for(vector<Point>::iterator pit = commonPath.begin(); pit != commonPath.end(); pit++){
                    pid2 = skeletonPoints->InsertNextPoint((*pit).x, (*pit).y, (*pit).z);
                    VTK_CREATE(vtkLine, line);
                    line->GetPointIds()->SetId(0, pid1);
                    line->GetPointIds()->SetId(1, pid2);
                    skeletonLines->InsertNextCell(line);
                    pid1 = pid2;
                }


                if(p2->info == nullptr){
                    pid2 = skeletonPoints->InsertNextPoint(p2->x, p2->y, p2->z);
                    cellpoints->InsertNextId(pid2);
                }else
                    pid2 = *static_cast<int*>(p2->info);

                VTK_CREATE(vtkLine, lastLine);
                line->GetPointIds()->SetId(0, pid1);
                line->GetPointIds()->SetId(1, pid2);
                skeletonLines->InsertNextCell(line);
            }

            for(int i = 0; i < cellpoints->GetNumberOfIds(); i++){
                vtkIdType id = cellpoints->GetId(i);
                skeletonNodesPoints->InsertNextPoint(skeletonPoints->GetPoint(id));
            }
            skeletonNodesData->SetPoints(skeletonNodesPoints);
            VTK_CREATE(vtkVertexGlyphFilter, filter1);
            filter1->SetInputData(skeletonNodesData);
            filter1->Update();
            VTK_CREATE(vtkVertexGlyphFilter, filter2);
            filter2->SetInputData(skeletonData);
            filter2->Update();


            skeletonData->SetPoints(skeletonPoints);
            skeletonData->SetLines(skeletonLines);
            VTK_CREATE(vtkPolyDataMapper, skeletonMapper);
            VTK_CREATE(vtkActor, skeletonActor);
            skeletonMapper->SetInputData(skeletonData);
            skeletonActor->SetMapper(skeletonMapper);
            skeletonActor->GetProperty()->SetLineWidth(2);
            sliceAssembly->AddPart(skeletonActor);
            skeletonActor->GetProperty()->SetColor(0, 0, 0);

            if(showArcNodes){
                VTK_CREATE(vtkPolyDataMapper, skeletonPointsMapper);
                VTK_CREATE(vtkActor, skeletonPointsActor);
                skeletonPointsMapper->SetInputConnection(filter2->GetOutputPort());
                skeletonPointsActor->SetMapper(skeletonPointsMapper);
                skeletonPointsActor->GetProperty()->SetColor(255, 0, 0);
                skeletonPointsActor->GetProperty()->SetRenderPointsAsSpheres(true);
                skeletonPointsActor->GetProperty()->SetPointSize(5);
                sliceAssembly->AddPart(skeletonPointsActor);
            }

            if(showNodes){
                VTK_CREATE(vtkPolyDataMapper, skeletonNodesMapper);
                VTK_CREATE(vtkActor, skeletonNodesActor);
                skeletonNodesMapper->SetInputConnection(filter1->GetOutputPort());
                skeletonNodesActor->SetMapper(skeletonNodesMapper);
                skeletonNodesActor->GetProperty()->SetColor(0, 0, 255);
                skeletonNodesActor->GetProperty()->SetPointSize(5);
                skeletonNodesActor->GetProperty()->SetRenderPointsAsSpheres(true);
                sliceAssembly->AddPart(skeletonNodesActor);
            }
        }
    }

    slicesCenter /= slices.size();
    sliceRen->AddActor(sliceAssembly);
    meshAssembly->Modified();
    ren->AddActor(meshAssembly);
    sliceRen->AddActor(sliceAssembly);
    double f[3] = {slicesCenter.x, slicesCenter.y, slicesCenter.z};
    sliceRen->GetActiveCamera()->SetFocalPoint(f);
    sliceRen->GetActiveCamera()->SetPosition(f[0], f[1] + 100, f[2]);
    sliceRen->Modified();
    this->ui->qvtkWidget->update();
    this->ui->slideWidget->update();

}

void MainWindow::updateIntersection(){

    Eigen::Vector4d omogeneousNormal = {0, 1, 0, 1};
    Eigen::Vector4d omogeneousCenter = {0, 0, 0, 1};
    Eigen::Vector4d omogeneousCenterResult = translationMatrix * omogeneousCenter;
    Eigen::Vector4d omogeneousNormalResult = zRotationMatrix * (yRotationMatrix * (xRotationMatrix * omogeneousNormal));
    Eigen::Matrix4d transformationMatrix = zRotationMatrix * (yRotationMatrix * (xRotationMatrix * translationMatrix));
    this->center.setValue(omogeneousCenterResult(0), omogeneousCenterResult(1), omogeneousCenterResult(2));
    this->normal.setValue(omogeneousNormalResult(0), omogeneousNormalResult(1), omogeneousNormalResult(2));
    slicer = new Slicer();
    slicer->setCenter(center);
    slicer->setNormal(normal);
    slicer->setMesh(model);
    slicer->setMaxSimplificationError(allowedError);
    slicer->setTransformationMatrix(transformationMatrix);
    slicer->slice();

    vector<Slice*> slices = slicer->getSlices();
    for(int i = 0; i < slices.size(); i++){

        MANode* skeleton = Utilities::medialAxisTransform(slices[i]->getSlice());
        slices[i]->setSkeleton(skeleton);
    }
    slicer->setSlices(slices);

    updateView();

}

void MainWindow::slotOpenFile(){

    QString filename = QFileDialog::getOpenFileName(nullptr,
                           "Choose a 3D model",
                           "./../../",
                           "STL(*.stl);;OBJ(*.obj);;PLY(*.ply);;All(*.*)");

    if (!filename.isEmpty()){

        std::cout << "loading: " << filename.toStdString() << std::endl;
        this->clear();

        if(model->load(filename.toStdString().c_str()) != 0){
            this->write("Can't open mesh file");
        }else{
            model->draw(meshAssembly);
            ren->AddActor(meshAssembly);
            this->ui->verticesLabel->setText(this->ui->verticesLabel->text() +
                                             QString::number(model->V.numels()));
            this->ui->edgesLabel->setText(this->ui->edgesLabel->text() +
                                             QString::number(model->E.numels()));
            this->ui->trianglesLabel->setText(this->ui->trianglesLabel->text() +
                                             QString::number(model->T.numels()));
            this->ui->actionClear->setEnabled(true);

            Point a, b;
            this->model->getBoundingBox(a, b);
            this->totalWidth = abs(a.x - b.x);
            this->totalHeight = abs(a.y - b.y);
            this->totalDepth = abs(a.z - b.z);
            this->ui->xSlider->setValue(0);
            this->ui->ySlider->setValue(0);
            this->ui->zSlider->setValue(0);
            this->ui->xSlider2->setValue(50);
            this->ui->ySlider2->setValue(50);
            this->ui->zSlider2->setValue(50);
            sliceRen->GetActiveCamera()->SetPosition(center.x, center.y + 2 * totalHeight, center.z);
            sliceRen->GetActiveCamera()->SetViewUp(0, 0, 1);
            sliceRen->GetActiveCamera()->SetFocalPoint(center.x, center.y, center.z);

            this->ui->tabWidget->setEnabled(true);
            updateIntersection();

        }

    }
}


void MainWindow::slotClear(){
    this->clear();
    this->write(initial_instruction);
}



void MainWindow::on_xSlider_valueChanged(int value){

    xRotationMatrix = Utilities::xRotationMatrix(M_PI * value / 100);
    //normal = Utilities::rotateVector(normal, Utilities::xRotMat(M_PI * (value - previousXValue) / 100));
    //previousXValue = value;
    updateIntersection();

}

void MainWindow::on_ySlider_valueChanged(int value){

    yRotationMatrix = Utilities::yRotationMatrix(M_PI * value / 100);
    //normal = Utilities::rotateVector(normal, Utilities::yRotMat(M_PI * (value - previousYValue) / 100));
    //previousYValue = value;
    updateIntersection();

}

void MainWindow::on_zSlider_valueChanged(int value){

    zRotationMatrix = Utilities::zRotationMatrix(M_PI * value / 100);
    //normal = Utilities::rotateVector(normal, Utilities::zRotMat(M_PI * (value - previousZValue) / 100));
    //previousZValue = value;
    updateIntersection();

}

void MainWindow::on_xSlider2_valueChanged(int value){

    Point a, b;
    model->getBoundingBox(a, b);
    translationVector(0) = min(a.x, b.x) + (value * totalWidth) / 100;
    translationMatrix = Utilities::translationMatrix(translationVector);
    //center.y += (value - previousHeightValue) * totalHeight / 100;
    //previousHeightValue = value;
    updateIntersection();

}

void MainWindow::on_ySlider2_valueChanged(int value){

    Point a, b;
    model->getBoundingBox(a, b);
    translationVector(1) = min(a.y, b.y) + (value * totalHeight) / 100;
    translationMatrix = Utilities::translationMatrix(translationVector);
    //center.y += (value - previousHeightValue) * totalHeight / 100;
    //previousHeightValue = value;
    updateIntersection();

}

void MainWindow::on_zSlider2_valueChanged(int value){

    Point a, b;
    model->getBoundingBox(a, b);
    translationVector(2) = min(a.z, b.z) + (value * totalDepth) / 100;
    translationMatrix = Utilities::translationMatrix(translationVector);
    //center.y += (value - previousHeightValue) * totalHeight / 100;
    //previousHeightValue = value;
    updateIntersection();

}

void MainWindow::on_showNodesBox_stateChanged(int arg1)
{
    if(arg1)
        this->showNodes = true;
    else
        this->showNodes = false;
    this->updateView();
}

void MainWindow::on_showArcNodesBox_stateChanged(int arg1)
{
    if(arg1)
        this->showArcNodes = true;
    else
        this->showArcNodes = false;
    this->updateView();
}

void MainWindow::on_maxErrorSlider_valueChanged(int value)
{
    this->allowedError = MAX_ERROR * ((double) value) / 100.0;
    updateIntersection();
}

void MainWindow::on_showSlices_stateChanged(int arg1)
{
    this->showSliceOnMesh = arg1;
    this->updateView();
}

void MainWindow::on_showBoundingBox_stateChanged(int arg1)
{
    this->showBoundingBox = arg1;
    this->updateView();
}

void MainWindow::on_showFittedCircle_stateChanged(int arg1)
{
    this->showFittedCircle = arg1;
    this->updateView();
}

void MainWindow::on_showSkeleton_stateChanged(int arg1)
{
    this->showSkeleton = arg1;
    this->updateView();
}

void MainWindow::on_showConvexHull_stateChanged(int arg1)
{
    this->showConvexHull = arg1;
    this->updateView();
}

void MainWindow::on_showboundingBox_stateChanged(int arg1)
{
    this->showBoundingRectangle = arg1;
    this->updateView();
}

void MainWindow::on_showDiagonals_stateChanged(int arg1)
{
    this->showDiagonals = arg1;
    this->updateView();
}


void MainWindow::on_showCenters_stateChanged(int arg1)
{
    this->showCenters = arg1;
    this->updateView();
}
