#include "slice.h"

Slice::Slice(){

    Point a(0, 0, 0);
    vector<Point> b = {a, a, a, a};
    this->boundingBox.insert(this->boundingBox.end(), b.begin(), b.end());
    this->minDiagonal = make_pair(a,a);
    this->maxDiagonal = make_pair(a,a);

}

void Slice::addHole(const std::vector<T_MESH::Point> hole)
{
    this->holes.push_back(hole);
}

void Slice::triangulate(){

    TriangleHelper helper(boundary, holes);
    slice = helper.getSliceMesh();
    slice->fixConnectivity();
    slice->buildVTKStructure();
    slice->setMeshModified(true);
    slice->setIsCage(false);
    slice->update();

}

DrawableMesh *Slice::getSlice() const
{
    return slice;
}

AndreasStructures::MANode *Slice::getSkeleton() const
{
    return skeleton;
}

void Slice::setSkeleton(AndreasStructures::MANode *value)
{
    skeleton = value;
}

void Slice::printInformation(){

    getBoundingBox();
    cout<<"Eccentricity: "<< (maxDiagonal.second - maxDiagonal.first).length() / (minDiagonal.second - minDiagonal.first).length()<<endl<<flush;
    getBestFittedCircles();
    Point center = slice->getCenter();
    Point circleCenter = bestFittedCircles[0].first;
    cout<<"Distance between ideal and real center: "<<(center - circleCenter).length()<<endl<<flush;
    vector<double> distances;
    for(unsigned int i  = 0; i < holes.size(); i++)
        distances.push_back(0);

    for(unsigned int i  = 0; i < boundary.size(); i++){
        for(unsigned int j = 0; j < holes.size(); j++){
            double bestDistance = DBL_MAX;
            for(unsigned k = 0; k < holes[j].size(); k++){
                double distance = (boundary[i] - holes[j][k]).length();
                if(distance < bestDistance){
                    bestDistance = distance;

                }

            }
            distances[j] += bestDistance;
        }
    }

    for(unsigned int i  = 0; i < holes.size(); i++){
        distances[i] /= boundary.size();
        cout<<"Mean distance from "<<i + 1<<"th hole: "<< distances[i]<<endl<<flush;
    }

}

vector<Point> Slice::getBoundingBox() const{

    vector<Eigen::Vector2d> points = Utilities::imatiToEigen2DPointRepresentation(boundary);

    vector<Eigen::Vector2d> boundingBox = Utilities::get2DOBB(points);

    this->boundingBox.clear();
    for(int i = 0; i < boundingBox.size(); i++){
        Point p(boundingBox[i](0), boundary[0].y, boundingBox[i](1));
        this->boundingBox.push_back(p);
    }

    Eigen::Vector2d mp1 = (boundingBox[1] + boundingBox[0]) / 2;
    Eigen::Vector2d mp2 = (boundingBox[2] + boundingBox[1]) / 2;
    Eigen::Vector2d mp3 = (boundingBox[3] + boundingBox[2]) / 2;
    Eigen::Vector2d mp4 = (boundingBox[0] + boundingBox[3]) / 2;

    Point p1(mp1(0), boundary[0].y, mp1(1));
    Point p2(mp2(0), boundary[0].y, mp2(1));
    Point p3(mp3(0), boundary[0].y, mp3(1));
    Point p4(mp4(0), boundary[0].y, mp4(1));

    if((mp3 - mp1).norm() < (mp4 - mp2).norm()){

        minDiagonal = make_pair(p1, p3);
        maxDiagonal = make_pair(p2, p4);

    }else{

        minDiagonal = make_pair(p2, p4);
        maxDiagonal = make_pair(p1, p3);

    }

    return this->boundingBox;
}

vector<Point> Slice::getConvexHull() const
{
    if(convexHull.size() == 0){

        vector<Eigen::Vector2d> points = Utilities::imatiToEigen2DPointRepresentation(boundary);
        vector<Eigen::Vector2d> convexHull = Utilities::extractConvexHull(points);
        for(int i = 0; i < convexHull.size(); i++){
            Point p(convexHull[i](0), boundary[0].y, convexHull[i](1));
            this->convexHull.push_back(p);
        }

    }

    return convexHull;
}

pair<Point, Point> Slice::getMinDiagonal() const{
    return minDiagonal;
}

pair<Point, Point> Slice::getMaxDiagonal() const{
    return maxDiagonal;
}

Eigen::Matrix4d Slice::getFromPlaneTransformation() const
{
    return fromPlaneTransformation;
}

void Slice::setFromPlaneTransformation(const Eigen::Matrix4d &value)
{
    fromPlaneTransformation = value;
}

std::vector<IMATI_STL::Point> Slice::getBoundary() const
{
    return boundary;
}

void Slice::setBoundary(const std::vector<IMATI_STL::Point> &value)
{
    boundary = value;
}

std::vector<std::vector<IMATI_STL::Point> > Slice::getHoles() const
{
    return holes;
}

void Slice::setHoles(const std::vector<std::vector<IMATI_STL::Point> > &value)
{
    holes = value;
}

vector<pair<Point, double> > Slice::getBestFittedCircles() const
{
    if(bestFittedCircles.size() == 0){

        vector<Eigen::Vector2d> boundaryPoints = Utilities::imatiToEigen2DPointRepresentation(boundary);
        pair<Eigen::Vector2d, double> br = Utilities::circleFitting(boundaryPoints);
        Point boundaryCircleCenter(br.first(0), boundary[0].y, br.first(1));
        bestFittedCircles.push_back(make_pair(boundaryCircleCenter, br.second));

        for(unsigned int i = 0; i < holes.size(); i++){

            vector<Eigen::Vector2d> holePoints = Utilities::imatiToEigen2DPointRepresentation(holes[i]);
            pair<Eigen::Vector2d, double> hr = Utilities::circleFitting(holePoints);
            Point holeCircleCenter(hr.first(0), boundary[0].y, hr.first(1));
            bestFittedCircles.push_back(make_pair(holeCircleCenter, hr.second));

        }
    }

    return bestFittedCircles;
}

