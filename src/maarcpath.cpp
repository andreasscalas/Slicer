#include "maarcpath.h"

using namespace AndreasStructures;
using namespace IMATI_STL;
using namespace std;

MAArcPath::MAArcPath()
{

}

void MAArcPath::addPoint(const Point &point)
{
    this->path.push_back(point);

}

vector<Point> MAArcPath::getPath() const
{
    return path;
}

void MAArcPath::setPath(const vector<Point> &value)
{
    path = value;
}

AndreasStructures::Node *MAArcPath::getN1() const
{
    return n1;
}

void MAArcPath::setN1(AndreasStructures::Node *value)
{
    n1 = value;
}

AndreasStructures::Node *MAArcPath::getN2() const
{
    return n2;
}

void MAArcPath::setN2(AndreasStructures::Node *value)
{
    n2 = value;
}
