#include "slicer.h"

Slicer::Slicer(){
    transformationMatrix << 1, 0, 0, 0,
                            0, 1, 0, 0,
                            0, 0, 1, 0,
                            0, 0, 0, 1;
}

void Slicer::slice(){

    VTK_CREATE(vtkPlane, intersectingPlane);
    intersectingPlane->SetOrigin(center.x, center.y, center.z);
    intersectingPlane->SetNormal(normal.x, normal.y, normal.z);

    VTK_CREATE(vtkCutter, cutter);
    cutter->SetCutFunction(intersectingPlane);
    VTK_CREATE(vtkPolyData, poly);
    poly->SetPoints(mesh->getPoints());
    poly->SetPolys(mesh->getTriangles());
    cutter->SetInputData(poly);
    cutter->Update();
    VTK_CREATE(vtkPolyData, pd);
    pd = cutter->GetOutput();

    /*******************************I punti estratti dal cutter sono i punti di intersezione tra piano ed edge del modello****************************/
    map<Vertex*, Vertex*> boundaryLines;
    map<int, Vertex*> intersectionPoints;
    vector<vector<Vertex*> > boundaries;
    pd->BuildCells();
    pd->BuildLinks();
    pd->GetLines()->InitTraversal();
    VTK_CREATE(vtkIdList, idList);

    while(pd->GetLines()->GetNextCell(idList)){
        double p1[3], p2[3];
        Vertex* v1, *v2;
        int id1 = idList->GetId(0), id2 = idList->GetId(1);

        if(intersectionPoints.find(id1) == intersectionPoints.end()){
            pd->GetPoint(id1, p1);
            v1 = new Vertex(p1[0], p1[1], p1[2]);
            intersectionPoints.insert(make_pair(id1,v1));
        }else
            v1 = intersectionPoints[id1];

        if(intersectionPoints.find(id2) == intersectionPoints.end()){
            pd->GetPoint(id2, p2);
            v2 = new Vertex(p2[0], p2[1], p2[2]);
            intersectionPoints.insert(make_pair(id2,v2));
        }else
            v2 = intersectionPoints[id2];

        boundaryLines.insert(make_pair(v1, v2));
    }

    map<Vertex*, Vertex*>::iterator bit = boundaryLines.begin();
    vector<Vertex*> boundary;
    Vertex* initialVertex = (*bit).first;

    while(boundaryLines.size() > 0){

        pair<Vertex*, Vertex*> p = (pair<Vertex*, Vertex*>) *bit;
        double epsilon = 1E-10;

        Point r = (*(p.first)) - (*(p.second));
        boundaryLines.erase(bit);
        if(abs(r.x) < epsilon && abs(r.y) < epsilon && abs(r.z) < epsilon){
            bit = boundaryLines.find(p.second);
            continue;
        }

        boundary.push_back(p.first);

        if(p.second == initialVertex ){


            boundary.push_back(p.second);
            boundaries.push_back(boundary);
            boundary.clear();
            if(boundaryLines.size() < 10)
                break;
            bit = boundaryLines.begin();
            initialVertex = (*bit).first;

        }else{
            bit = boundaryLines.find(p.second);
            if(bit == boundaryLines.end())
                break;
        }

    }

    boundary.clear();

    for(vector<vector<Vertex*> >::iterator bit = boundaries.begin(); bit != boundaries.end(); bit++){

        double minAngle = DBL_MAX;
        unsigned int newStartingPosition = 0;
        vector<Vertex*> contour = (vector<Vertex*>) *bit;
        contour.erase(contour.end() - 1);

        for(unsigned int i = 0; i < contour.size(); i++){
            Vertex* v1 = contour[Utilities::mod((i - 1), contour.size())];
            Vertex* v2 = contour[i];
            Vertex* v3 = contour[(i + 1) % (contour.size())];
            double angle = v2->getAngle(v1, v3);
            if(angle< minAngle){
                newStartingPosition = i;
                minAngle = angle;
            }
        }

        rotate(contour.begin(), contour.begin() + newStartingPosition, contour.end());

        *bit = Utilities::simplifyLine(contour, maxSimplificationError);
    }

    vector<vector<Point> > boundaries2D;
    for(unsigned int i = 0; i < boundaries.size(); i++){
        vector<Point> boundary2D = Utilities::transformVertexList(boundaries[i], transformationMatrix.inverse());
        boundaries2D.push_back(boundary2D);
    }

    vector<ContourContainmentTreeNode* > nodes;
    int i;
    for(i = 0; i < boundaries2D.size(); i++){
        nodes.push_back(new ContourContainmentTreeNode());
        nodes.back()->setData(&(boundaries2D[i]));
    }

    for(i = 0; i < boundaries2D.size(); i++){

        for(int j = 0; j < boundaries2D.size(); j++){
            if(i == j)
                continue;

            if(Utilities::isPointInsidePolygon(boundaries2D[j][0], boundaries2D[i])){
                nodes[i]->addSon(nodes[j]);
                nodes[j]->addContainer(nodes[i]);
            }
        }

    }

    for(i = 0; i < nodes.size(); i++)
        if(nodes[i]->getContainers().size() == 0)
            nodes[i]->removeSonsCycles();


    for(i = 0; i < nodes.size(); i++)
        if(nodes[i]->getContainers().size() == 0){

            nodes[i]->reorderBoundaries(0);
            vector<Slice*> contained = nodes[i]->getContainedSlices();
            slices.insert(slices.end(), contained.begin(), contained.end());
            for(int i = 0; i < slices.size(); i++){

                slices[i]->setFromPlaneTransformation(transformationMatrix);
                slices[i]->triangulate();

            }

        }
}

IMATI_STL::Point Slicer::getNormal() const{
    return normal;
}

void Slicer::setNormal(const IMATI_STL::Point &value)
{
    normal = value;
}

IMATI_STL::Point Slicer::getCenter() const
{
    return center;
}

void Slicer::setCenter(const IMATI_STL::Point &value)
{
    center = value;
}

DrawableMesh *Slicer::getMesh() const
{
    return mesh;
}

void Slicer::setMesh(DrawableMesh *value)
{
    mesh = value;
}

std::vector<Slice*> Slicer::getSlices() const
{
    return slices;
}

void Slicer::setSlices(const std::vector<Slice*> &value)
{
    slices = value;
}

double Slicer::getMaxSimplificationError() const
{
    return maxSimplificationError;
}

void Slicer::setMaxSimplificationError(double value)
{
    maxSimplificationError = value;
}

Eigen::Matrix4d Slicer::getTransformationMatrix() const
{
    return transformationMatrix;
}

void Slicer::setTransformationMatrix(const Eigen::Matrix4d &value)
{
    transformationMatrix = value;
}
