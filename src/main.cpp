#include "mainwindow.h"
#include "imatistl.h"
#include "drawablemesh.h"
#include "utilities.h"
#include "annotation.h"
#include <eigen3/Eigen/Dense>
#include <QApplication>
#include <sstream>

int main(int argc, char *argv[]){

    /*if(argc > 1){
        ImatiSTL::init();
        DrawableMesh* mesh = new DrawableMesh();
        if(mesh->load(argv[1]) == 0){
            IMATI_STL::Point normal = {0, 1, 0};
            IMATI_STL::Point a, b, c, axis(0,0,0);
            mesh->getBoundingBox(a, b);
            double totalHeight = abs(a.y - b.y) * 2 / 5;
            double min = std::min(a.y, b.y);
            double circularError = 0;
            double eccentricity = 0;
            double slicesHoleDistance = 0;
            vector<double> slicesThickness;
            int holeCount = 0;
            c = mesh->getCenter();
            vector<Point> barycenters;

            for(unsigned int i = 0; i < 100; i++){
                c.y = min + totalHeight * i / 100;
                Slicer slicer;
                slicer.setCenter(c);
                slicer.setNormal(normal);
                slicer.setMaxSimplificationError(0);
                slicer.setMesh(mesh);
                slicer.slice();

                vector<Slice*> slices = slicer.getSlices();
                double sliceCircularError = 0;
                if(slices.size() == 1 && i > 10 && i < 30){
                    Point center(0,0,0);
                    Slice* s = slices[0];
                    vector<vector<Point> > holes = s->getHoles();
                    if(s->getHoles().size() == 1){
                        double holeDistance = 0;
                        vector<Point> boundary = s->getBoundary();
                        for(unsigned int j  = 0; j < boundary.size(); j++){
                            double bestDistance = DBL_MAX;
                            for(unsigned k = 0; k < holes[0].size(); k++){
                                double distance = (boundary[j] - holes[0][k]).length();
                                if(distance < bestDistance){
                                    bestDistance = distance;

                                }
                            }
                            holeDistance += bestDistance;
                        }

                        holeDistance  /= boundary.size();
                        slicesThickness.push_back(holeDistance);
                        slicesHoleDistance += holeDistance;
                        holeCount++;
                    }
                    vector<pair<Point, double> > circles = s->getBestFittedCircles();
                    s->getBoundingBox();
                    pair<Point, Point> minDiagonal = s->getMinDiagonal();
                    pair<Point, Point> maxDiagonal = s->getMaxDiagonal();
                    eccentricity += (maxDiagonal.second - maxDiagonal.first).length() / (minDiagonal.second - minDiagonal.first).length();
                    vector<Point> boundary = s->getBoundary();
                    for(unsigned int i = 0; i < boundary.size(); i++){
                        center += boundary[i];
                        sliceCircularError += abs((boundary[i] - circles[0].first).length() - circles[0].second);
                    }

                    center /= boundary.size();
                    axis += center;
                    barycenters.push_back(center);

                    sliceCircularError /= boundary.size();
                    circularError += sliceCircularError;
                }

            }

            //vector<Eigen::Vector3d> blist = Utilities::imatiToEigen3DPointRepresentation(barycenters);
            //pair<Eigen::Vector3d, Eigen::Vector3d> line = Utilities::linearRegression(blist);

            //cout<<"Principal axis: u=("<<line.first(0)<<","<<line.first(1)<<","<<line.first(0)<<");";
            //cout<<"P0=("<<line.second(0)<<","<<line.second(1)<<","<<line.second(0)<<");"<<endl<<flush;
            double err = 0;
            axis /= barycenters.size();
            for(unsigned int i = 0; i < barycenters.size(); i++){
                Eigen::Vector2d axisPoint = {axis.x, axis.z};
                Eigen::Vector2d barycenter = {barycenters[i].x, barycenters[i].z};
                err += (barycenter - axisPoint).norm();
            }

            double thicknessError = 0;
            for(unsigned int i = 0; i < slicesThickness.size(); i++)
                thicknessError += slicesThickness[i];

            err /= barycenters.size();
            circularError /= 100;
            eccentricity /= 100;
            slicesHoleDistance /= holeCount;
            thicknessError /= slicesThickness.size();


            cout<<err<<", "<<circularError<<", "<<eccentricity<<", "<<thicknessError<<flush;

        }




    }
    */
    /*vector<Eigen::Vector3d> points = {
        {-10,  -10,  -67},
        {-9,   -9,  -60},
        {-8,   -8,  -53},
        {-7,   -7,  -46},
        {-6,   -6,  -39},
        {-5,   -5,  -32},
        {-4,   -4,  -25},
        {-3,   -3,  -18},
        {-2,   -2,  -11},
        {-1,   -1,   -4},
        {0,    0,    3},
        {1,    1,   10},
        {2,    2,   17},
        {3,    3,   24},
        {4,    4,   31},
        {5,    5,   38},
        {6,    6,   45},
        {7,    7,   52},
        {8,    8,   59},
        {9,    9,   66},
        {10,   10,   73}

    };

    std::pair<Eigen::Vector3d,Eigen::Vector3d> p = Utilities::linearRegression(points);

    srand(time(NULL));
    double min = -100;
    for(int i = 0; i < 1000; i++){
        Eigen::Vector3d pu = p.second + p.first * (min + ((double) i) / 10.0);
        cout<<pu(0)<<","<<pu(1)<<","<<pu(2)<<";"<<endl<<flush;
    }*/
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
    //return 0;
}
