#include "trianglehelper.h"

using namespace std;
using namespace IMATI_STL;
TriangleHelper::TriangleHelper(std::vector<IMATI_STL::Point> boundary, std::vector<std::vector<IMATI_STL::Point> > holes)
{
    sliceMesh = new DrawableMesh();
    this->boundary.insert(this->boundary.end(), boundary.begin(), boundary.end());
    this->holes.insert(this->holes.end(), holes.begin(), holes.end());
    writePoly();
    system("/media/andreas/Volume/Progetti/triangle/triangle -pQ tmp.poly");
    loadELE();
}


DrawableMesh *TriangleHelper::getSliceMesh() const{
    return sliceMesh;
}

void TriangleHelper::writePoly(){
    ofstream myfile;
    myfile.open(filename + ".poly");
    long verticesNumber = 0;
    long vid = 1;
    vector<pair<long, long> > segments;

    verticesNumber += boundary.size() - 1;
    for(vector<vector<Point> >::iterator bit = holes.begin(); bit != holes.end(); bit++){
        vector<Point> boundary = (*bit);
        verticesNumber += boundary.size() - 1;
    }

    myfile<< verticesNumber <<" 2 0 0"<< " #Vertices number \n";
    for(vector<Point>::iterator vit = boundary.begin(); vit != boundary.end() - 1; vit++){
        myfile<<vid<<" "<<(*vit).x<<" "<<(*vit).z<<"\n";
        idVertex[vid] = &(*vit);
        if(vid > 1)
            segments.push_back(make_pair(vid - 1, vid));
        vid++;
    }
    segments.push_back(make_pair(vid - 1, 1));
    long reached_vid = vid;
    for(vector<vector<Point> >::iterator bit = holes.begin(); bit != holes.end(); bit++){
        vector<Point> outline = (*bit);
        for(vector<Point>::iterator vit = outline.begin(); vit != outline.end() - 1; vit++){
            myfile<<vid<<" "<<(*vit).x<<" "<<(*vit).z<<"\n";
            idVertex[vid] = &(*vit);
            if(vid > reached_vid)
                segments.push_back(make_pair(vid - 1, vid));
            vid++;
        }
        segments.push_back(make_pair(vid - 1, reached_vid));
        reached_vid = vid;
    }
    long sid = 1;
    myfile<< segments.size()<< " #Segments number \n";
    for(vector<pair<long, long> >::iterator sit = segments.begin(); sit != segments.end(); sit++){
        myfile<<sid<<" "<<(*sit).first<<" "<<(*sit).second<<"\n";
        sid++;
    }

    long hid = 1;
    myfile<< holes.size()<< " #Holes number \n";
    for(vector<vector<Point> >::iterator bit = holes.begin(); bit != holes.end(); bit++){
        vector<Point> outline = (*bit);
        Point v = ((outline[1]) - (outline[0])) * 1E-3;
        double vec[2] = {-v.z, v.x};
        Point middle = ((outline[1]) + (outline[0])) / 2;
        double innerPoint[2] = {vec[0] + middle.x, vec[1] + middle.z};
        double turningSign =    (outline[1].z - outline[0].z) * (innerPoint[0] - outline[0].x) -
                                (outline[1].x - outline[0].x) * (innerPoint[1] - outline[0].z);
        if(turningSign < 0){
            innerPoint[0] = -vec[0] + middle.x;
            innerPoint[1] = -vec[1] + middle.z;
        }

        myfile<<hid<<" "<<innerPoint[0]<<" "<<innerPoint[1]<<"\n";
        hid++;
    }

    myfile.close();
}

void TriangleHelper::loadELE(){
    ifstream myfile;
    string line;

    myfile.open(filename + ".1.node");
    if (myfile.is_open()){
        stringstream stream;
        int verticesNumber;
        getline (myfile, line);
        stream << line;
        stream>>verticesNumber;
        for(int i = 0; i < verticesNumber; i++){
            getline (myfile,line);
            stream = std::stringstream();
            stream << line;
            int vid;
            double x, y;
            stream >> vid;
            stream >> x;
            stream >> y;
            Vertex* v = new Vertex(x, boundary[0].y, y);
            sliceMesh->V.appendTail(v);
            idVertex[vid] = v;
        }
    }

    myfile.close();

    myfile.open(filename + ".1.ele");

    if (myfile.is_open()){
        stringstream stream;
        int trianglesNumber;
        getline (myfile, line);
        stream << line;
        stream >> trianglesNumber;
        for(int i = 0; i < trianglesNumber; i++){
            getline (myfile, line);
            stream = std::stringstream();
            stream << line;
            int tid, vid1, vid2, vid3;
            stream >> tid;
            stream >> vid1;
            stream >> vid2;
            stream >> vid3;
            ExtVertex* v1 = new ExtVertex(idVertex[vid1]);
            ExtVertex* v2 = new ExtVertex(idVertex[vid2]);
            ExtVertex* v3 = new ExtVertex(idVertex[vid3]);

            sliceMesh->CreateTriangleFromVertices(v1, v2, v3);

        }
        myfile.close();
        sliceMesh->mergeCoincidentEdges();
        sliceMesh->savePLY("prova.ply");
    }
}
