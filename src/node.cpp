#include "node.h"
using namespace AndreasStructures;

Node::Node(){
}

std::vector<Node *> Node::getConnectedNodes() const
{
    return connectedNodes;
}

void Node::setConnectedNodes(const std::vector<Node *> &value)
{
    connectedNodes = value;
}

void Node::addConnectedNode(Node *value)
{
    connectedNodes.push_back(value);
}

void *Node::getData() const
{
    return data;
}

void Node::setData(void *value)
{
    data = value;
}

int Node::getKey() const
{
    return key;
}

void Node::setKey(int value)
{
    key = value;
}
