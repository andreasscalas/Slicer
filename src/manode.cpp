#include "manode.h"

using namespace AndreasStructures;
using namespace std;
using namespace IMATI_STL;
MANode::MANode(){
    markedFlag = false;
    visitedFlag = false;
}

IMATI_STL::Point* MANode::getPoint() const
{
    return point;
}

void MANode::setPoint(const IMATI_STL::Point value)
{
    point =  new Point(value);
}

std::vector<AndreasStructures::MAArcPath *> MANode::getPaths() const
{
    return paths;
}

void MANode::setPaths(const std::vector<AndreasStructures::MAArcPath *> &value)
{
    paths = value;
}

void MANode::addPath(AndreasStructures::MAArcPath* path){

    this->paths.push_back(path);

}


bool MANode::getMarkedFlag() const
{
    return markedFlag;
}

void MANode::setMarkedFlag(bool value)
{
    markedFlag = value;
}

bool MANode::getVisitedFlag() const
{
    return visitedFlag;
}

void MANode::setVisitedFlag(bool value)
{
    visitedFlag = value;
}

std::vector<std::pair<MANode *, MANode *> > MANode::getGraphArcs(){

    vector<MANode*> toWalkNodes = {this};
    vector<pair<MANode*, MANode*> > arcs;

    while(toWalkNodes.size() > 0){
        MANode* current = toWalkNodes.back();
        vector<AndreasStructures::Node*> connected = current->getConnectedNodes();
        toWalkNodes.pop_back();
        current->setMarkedFlag(true);
        for(vector<AndreasStructures::Node*>::iterator cit = connected.begin(); cit != connected.end(); cit++){

            MANode* connectedNode = (MANode*) (*cit);
            if(connectedNode->getMarkedFlag() == false){

                arcs.push_back(make_pair(current, connectedNode));
                if(connectedNode->getVisitedFlag() == false){

                    connectedNode->setVisitedFlag(true);
                    toWalkNodes.push_back(connectedNode);

                }

            }

        }

    }

    for(int i = 0; i < arcs.size(); i++){

        pair<MANode*, MANode*> p = (arcs[i]);
        p.first->setMarkedFlag(false);
        p.second->setMarkedFlag(false);
        p.first->setVisitedFlag(false);
        p.second->setVisitedFlag(false);

    }

    return arcs;

}

std::vector<Point> MANode::getCommonPath(MANode *other){

    vector<MAArcPath*> n2Paths = other->getPaths();
    vector<Point> voidVector;

    for(vector<MAArcPath*>::iterator pit1 = this->paths.begin(); pit1 != this->paths.end(); pit1++)
        for(vector<MAArcPath*>::iterator pit2 = n2Paths.begin(); pit2 != n2Paths.end(); pit2++)
            if((*pit1) == (*pit2)){
                vector<Point> path = (*pit1)->getPath();
                if((*pit1)->getN1() != this)
                    reverse(path.begin(), path.end());
                return path;
            }

    return voidVector;

}
