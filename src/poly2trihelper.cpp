#include <poly2trihelper.h>
using namespace std;
using namespace IMATI_STL;

Poly2TriHelper::Poly2TriHelper(std::vector<std::vector<IMATI_STL::Vertex*> > boundaries){
    sliceMesh = new DrawableMesh();
    this->boundaries.insert(this->boundaries.end(), boundaries.begin(), boundaries.end());
    writeBDM();
    system("../../poly2tri/poly2tri -smp tmp.bdm");
    loadELE();
}


DrawableMesh *Poly2TriHelper::getSliceMesh() const
{
    return sliceMesh;
}

void Poly2TriHelper::writeBDM(){
    ofstream myfile;
    myfile.open(filename + ".bdm");
    long vid = 0;
    for(vector<vector<Vertex*> >::iterator bit = boundaries.begin(); bit != boundaries.end(); bit++){
        vector<Vertex*> boundary = (*bit);
        myfile<< boundary.size() - 1 << "\n";
        for(vector<Vertex*>::iterator vit = boundary.begin(); vit != boundary.end() - 1; vit++){
            myfile<<(*vit)->x<<" "<<(*vit)->z<<"\n";
            idVertex[vid] = (*vit);
            vid++;
        }
        myfile<<"\n";
    }
    myfile.close();
}

void Poly2TriHelper::loadELE(){
    ifstream myfile;
    string line;

    myfile.open(filename + ".node");
    if (myfile.is_open()){
        getline (myfile,line);
        getline (myfile,line);
        while ( getline (myfile,line) ){
            stringstream stream;
            stream << line;
            int vid;
            double x, y;
            stream>>vid;
            stream>>x;
            stream>>y;
            Vertex* v = new Vertex(x, boundaries[0][0]->y, y);
            sliceMesh->V.appendTail(v);
            idVertex[vid] = (long) v;
        }
    }
    myfile.close();

    myfile.open(filename + ".ele");

    if (myfile.is_open()){
        getline (myfile,line);
        getline (myfile,line);
        while ( getline (myfile,line) ){
            stringstream stream;
            stream << line;
            long tid, vid1, vid2, vid3;
            stream>>tid;
            stream>>vid1;
            stream>>vid2;
            stream>>vid3;
            ExtVertex* v1 = new ExtVertex(idVertex[vid1]);
            ExtVertex* v2 = new ExtVertex(idVertex[vid2]);
            ExtVertex* v3 = new ExtVertex(idVertex[vid3]);

            sliceMesh->CreateTriangleFromVertices(v1, v2, v3);

        }
        myfile.close();
    }
}
