#include "utilities.h"

#include <MathGeoLib/MathGeoLib.h>
#include <MathGeoLib/Math/myassert.h>
#include <MathGeoLib/Algorithm/GJK.h>
#include <MathGeoLib/Algorithm/SAT.h>

using namespace IMATI_STL;
using namespace Eigen;
using namespace std;
using namespace AndreasStructures;

namespace Utilities{

    int mod(int val, int m){ return (val % m + m) % m; }

    bool isPointInsidePolygon(Point v, std::vector<Point> boundary){

        int cn = 0;    // the  crossing number counter

        // loop through all edges of the polygon
        for (int i = 0; i < boundary.size() - 1; i++) {    // edge from boundary[i]  to boundary[i+1]
           if (((boundary[i].z <= v.z) && (boundary[i+1].z > v.z))     // an upward crossing
            || ((boundary[i].z > v.z) && (boundary[i+1].z <=  v.z))) { // a downward crossing
                // compute  the actual edge-ray intersect x-coordinate
                float vt = (float)(v.z  - boundary[i].z) / (boundary[i+1].z - boundary[i].z);
                if (v.x <  boundary[i].x + vt * (boundary[i+1].x - boundary[i].x)) // v.x < intersect
                     ++cn;   // a valid crossing of y=v.y right of v.x
            }
        }
        return (cn&1);    // 0 if even (out), and 1 if  odd (in)
    }

    IMATI_STL::Point rotateVector(IMATI_STL::Point v, Matrix4d rotationMatrix){
        Vector4d n = {v.x, v.y, v.z, 1};
        Vector4d n_ = rotationMatrix * n;
        Point newNormal(n_(0), n_(1), n_(2));
        return newNormal;
    }

    bool isBoundaryClockwise(std::vector<Point> boundary){

        double sum = 0.0;
        for (int i = 0; i < boundary.size(); i++) {
            Point v1 = boundary[i];
            Point v2 = boundary[(i + 1) % boundary.size()]; // % is the modulo operator
            sum += (v2.x - v1.x) * (v2.z + v1.z);
        }

        return sum > 0.0;

    }

    Matrix4d xRotationMatrix(double alfa){

        Eigen::Matrix4d xRot;

        xRot<<  1,          0,          0,          0,
                0,          cos(alfa),  sin(alfa),  0,
                0,          -sin(alfa), cos(alfa),  0,
                0,          0,          0,          1;

        return xRot;

    }

    Matrix4d yRotationMatrix(double alfa){

        Eigen::Matrix4d yRot;

        yRot<<  cos(alfa),  0,          -sin(alfa), 0,
                0,          1,          0,          0,
                sin(alfa),  0,          cos(alfa),  0,
                0,          0,          0,          1;

        return yRot;

    }

    Matrix4d zRotationMatrix(double alfa){

        Eigen::Matrix4d zRot;

        zRot<<  cos(alfa),  sin(alfa),  0,          0,
                -sin(alfa), cos(alfa),  0,          0,
                0,          0,          1,          0,
                0,          0,          0,          1;

        return zRot;

    }

    Matrix4d translationMatrix(Vector3d direction){

        Eigen::Matrix4d translation;

        translation<<   1,       0,      0,      direction(0),
                        0,       1,      0,      direction(1),
                        0,       0,      1,      direction(2),
                        0,       0,      0,      1;

        return translation;
    }

    vector<Edge*> getBoundaryEdges(IMATI_STL::Triangle* t){
        vector<Edge*> boundaryEdges;
        if(t->e1->isOnBoundary())
            boundaryEdges.push_back(t->e1);
        if(t->e2->isOnBoundary())
            boundaryEdges.push_back(t->e2);
        if(t->e3->isOnBoundary())
            boundaryEdges.push_back(t->e3);

        return boundaryEdges;
    }

    IMATI_STL::Triangle* getMAStartingTriangle(DrawableMesh *slice){
        List* VT = &(slice->T);
        IMATI_STL::Triangle* t, *starting, *lastBranching, *lastSimple;
        IMATI_STL::Node* n;
        bool found = false;

        FOREACHVTTRIANGLE(VT, t, n){
            short numBoundaryEdges = getBoundaryEdges(t).size();

            switch(numBoundaryEdges){
                case 3:
                case 2:
                    found = true;
                    starting = t;
                    break;
                case 1:
                    lastSimple = t;
                    break;
                case 0:
                    lastBranching = t;
                break;
                default:
                    ;
            }

            if(found)
                break;
        }

        if(starting == nullptr)
            if(lastBranching == nullptr)
                starting = lastSimple;
            else
                starting = lastBranching;

        return starting;
    }

    MANode* walkBranch(Edge *startingEdge, IMATI_STL::Triangle *startingTriangle, MANode *first, MAArcPath* path){

        MANode* returnNode = new MANode();
        IMATI_STL::Triangle* t = startingTriangle;
        Edge* e = startingEdge;
        int USED = 892892;

        bool ended = false;
        while(!ended){

            int constrainedEdgesNumber = getBoundaryEdges(t).size();
            switch(constrainedEdgesNumber){

                case 2:{

                    path->setN2(returnNode);
                    returnNode->addPath(path);
                    returnNode->addConnectedNode(first);
                    returnNode->setPoint(t->oppositeVertex(e));
                    ended = true;
                    break;

                }

                case 1:{

                    if(t->nextEdge(e)->isOnBoundary())
                        e = t->prevEdge(e);
                    else
                        e = t->nextEdge(e);

                    e->info = &USED;
                    path->addPoint(e->getMidPoint());
                    t = e->oppositeTriangle(t);
                    break;

                }

                case 0:{

                    if(t->info == nullptr){

                        path->setN2(returnNode);
                        returnNode->addPath(path);
                        returnNode->addConnectedNode(first);
                        returnNode->setPoint(t->getCenter());
                        t->info = (void*) returnNode;
                        Edge* e1 = t->nextEdge(e);

                        if(e1->info == nullptr || *static_cast<int*>(e1->info) != USED){

                            e1->info = &USED;
                            IMATI_STL::Triangle* t1 = e1->oppositeTriangle(t);
                            MAArcPath* path1 = new MAArcPath();
                            path1->setN1(returnNode);
                            path1->addPoint(e1->getMidPoint());
                            MANode* n1 = walkBranch(e1, t1, returnNode, path1);
                            returnNode->addPath(path1);
                            returnNode->addConnectedNode(n1);
                        }

                        Edge* e2 = t->prevEdge(e);
                        if(e2->info == nullptr || *static_cast<int*>(e2->info) != USED){

                            e2->info = &USED;
                            IMATI_STL::Triangle* t2 = e2->oppositeTriangle(t);
                            MAArcPath* path2 = new MAArcPath();
                            path2->setN1(returnNode);
                            path2->addPoint(e2->getMidPoint());
                            MANode* n2 = walkBranch(e2, t2, returnNode, path2);
                            returnNode->addPath(path2);
                            returnNode->addConnectedNode(n2);
                        }

                    }else{

                        MANode* n = static_cast<MANode*>(t->info);
                        path->setN2(returnNode);
                        returnNode = n;
                        returnNode->addPath(path);
                        returnNode->addConnectedNode(first);
                    }

                    ended = true;
                    break;

                }

                default:
                    exit(13);

            }
        }

        return returnNode;
    }


    MANode* medialAxisTransform(DrawableMesh* mesh){

        IMATI_STL::Triangle* startingTriangle = getMAStartingTriangle(mesh);
        int numOfConstrainedEdges = getBoundaryEdges(startingTriangle).size();
        MANode* skeleton = new MANode();
        Point* p;
        MANode* n;
        Point center = startingTriangle->getCenter();
        List* VT = &(mesh->T);
        IMATI_STL::Triangle* t;
        IMATI_STL::Node* n_;

        FOREACHVTTRIANGLE(VT, t, n_)
            t->info = nullptr;

        switch (numOfConstrainedEdges) {

            case 3:
                p = new Point();
                *p = center;
                skeleton->setPoint(p);
                break;

            case 2:

                MAArcPath* path = new MAArcPath();
                IMATI_STL::Triangle* t;
                Edge* e;
                if(!startingTriangle->e1->isOnBoundary())
                    e = startingTriangle->e1;
                else if(!startingTriangle->e2->isOnBoundary())
                    e = startingTriangle->e2;
                else if(!startingTriangle->e3->isOnBoundary())
                    e = startingTriangle->e3;
                skeleton->setPoint(*startingTriangle->oppositeVertex(e));
                t = e->oppositeTriangle(startingTriangle);
                p = new Point();
                path->setN1(skeleton);
                path->addPoint(e->getMidPoint());
                n = walkBranch(e, t, skeleton, path);
                skeleton->addConnectedNode(n);
                skeleton->addPath(path);
                break;

            case 1:

                *p = center;
                startingTriangle->info = (void*) skeleton;
                skeleton->setPoint(p);
                n = walkBranch(e, t, skeleton, path);

            case 0:

                *p = center;
                skeleton->setPoint(p);
                t->info = (void*) skeleton;
                Edge* e1 = startingTriangle->e1;
                Edge* e2 = startingTriangle->e2;
                Edge* e3 = startingTriangle->e3;
                IMATI_STL::Triangle* t1 = e1->oppositeTriangle(startingTriangle);
                IMATI_STL::Triangle* t2 = e2->oppositeTriangle(startingTriangle);
                IMATI_STL::Triangle* t3 = e3->oppositeTriangle(startingTriangle);
                MAArcPath* path1 = new MAArcPath();
                MAArcPath* path2 = new MAArcPath();
                MAArcPath* path3 = new MAArcPath();
                path1->addPoint(e1->getMidPoint());
                MANode* n1 = walkBranch(e1, t1, skeleton, path1);
                if(n1 != skeleton){
                    skeleton->addPath(path1);
                    skeleton->addConnectedNode(n1);
                }

                if(t2->info == nullptr){
                    path2->addPoint(e2->getMidPoint());
                    MANode* n2 = walkBranch(e2, t2, skeleton, path2);
                    if(n2 != skeleton){
                        skeleton->addPath(path2);
                        skeleton->addConnectedNode(n2);
                    }
                }

                if(t3->info == nullptr){
                    path3->addPoint(e3->getMidPoint());
                    MANode* n3 = walkBranch(e3, t3, skeleton, path3);
                    if(n3 != skeleton){
                        skeleton->addPath(path3);
                        skeleton->addConnectedNode(n3);
                    }
                }

            default:
                break;
        }

        FOREACHVTTRIANGLE(VT, t, n_)
            t->info = nullptr;


        return skeleton;
    }

    std::vector<Vertex *> simplifyLine(std::vector<Vertex *> line, double maxError){

        unsigned int anchorPosition = 0;
        unsigned int floaterPosition = Utilities::mod(anchorPosition - 1, line.size());
        unsigned int  startingFloater = floaterPosition;
        Vertex* anchor = line[anchorPosition];
        Vertex* floater = line[floaterPosition];
        vector<Vertex*> newContour = {anchor};
        stack<unsigned int> floaters;
        floaters.push(floaterPosition);

        while(anchorPosition != startingFloater){

            double worstDistance = 0;
            int worstPosition = -1;

            for(unsigned int i = (anchorPosition + 1) % (line.size()); i != floaterPosition; i = (i + 1) % (line.size())){
                Vertex* v = line[i];
                double distance = v->distanceFromLine(anchor, floater);
                if(distance > maxError && distance > worstDistance){
                    worstPosition = i;
                    worstDistance = distance;
                }
            }

            if(worstPosition != -1){

                floaterPosition = worstPosition;
                floater = line[floaterPosition];
                floaters.push(floaterPosition);

            }else{

                anchorPosition = floaterPosition;
                anchor = line[anchorPosition];
                newContour.push_back(anchor);
                floaterPosition = floaters.top();
                if(floaters.top() == anchorPosition)
                    floaters.pop();

                if(floaters.size() > 0)
                    floaterPosition = floaters.top();
                else
                    break;

                floater = line[floaterPosition];

            }


        }

        line.push_back(line.front());
        newContour.push_back(newContour.front());
        return newContour;

    }

    std::pair<double*, double> circleFitting(std::vector<double*> points){

        double xc = 7;
        double yc = 5;
        double r = 0;
        double fold = DBL_MAX;
        double fnew = DBL_MAX;

        double alpha = 0.5;
        do{
            fold = fnew;
            double xcgradient = 0, ycgradient = 0, rgradient = 0;
            for(int i = 1; i < points.size(); i++){
                double distanceFromCenter = sqrt(pow(points[i][0] - xc, 2) + pow(points[i][1] - yc, 2));
                rgradient += r - distanceFromCenter;
                xcgradient += (points[i][0] - xc) * (r - distanceFromCenter) / distanceFromCenter;
                ycgradient += (points[i][1] - yc) * (r - distanceFromCenter) / distanceFromCenter;
            }

            r -= alpha * rgradient / points.size();
            xc -= alpha * xcgradient / points.size();
            yc -= alpha * ycgradient / points.size();
            fnew = 0;
            for(int i = 0; i < points.size(); i++){
                fnew += pow(r - sqrt(pow(points[i][0] - xc, 2) + pow(points[i][1] - yc, 2)), 2);
            }

            fnew /= 2 * points.size();

        }while(fold > fnew + EPSILON);

        double* center = malloc(2 * sizeof(double));
        center[0] = xc;
        center[1] = yc;
        return std::make_pair(center, r);

    }

    std::pair<Eigen::Vector2d, double> circleFitting(std::vector<Vector2d> points){

        Vector2d center = {0, 0};
        double radius = 0;
        double fold = DBL_MAX;
        double fnew = DBL_MAX;

        for(unsigned int i = 1; i < points.size(); i++)
            center += points[i];
        center /= points.size();
        radius = (points[0] - center).norm();

        double alpha = STEP_SIZE;
        do{
            fold = fnew;
            Vector2d cgradient = {0, 0};
            double rgradient = 0;
            for(unsigned int i = 1; i < points.size(); i++){

                double distanceFromCenter = (points[i] - center).norm();
                rgradient += radius - distanceFromCenter;
                cgradient += (points[i] - center) * (radius - distanceFromCenter) / distanceFromCenter;
            }

            radius -= alpha * rgradient / points.size();
            center -= alpha * cgradient / points.size();
            fnew = 0;

            for(unsigned int i = 0; i < points.size(); i++){
                fnew += pow(radius - (points[i] - center).norm(), 2);
            }

            fnew /= 2 * points.size();

        }while(fold > fnew + EPSILON);

        return std::make_pair(center, radius);

    }

    std::pair<Vector3d, Vector3d> linearRegression(std::vector<Vector3d> points){

        int m = points.size();
        Vector3d p0 = points[0];
        Vector3d u = {0, 1, 0};
        double t = 1;

        double alpha = 0.005;
        double fold = DBL_MAX;
        double fnew = DBL_MAX;
        do{

            Vector3d p0Gradient = {0, 0, 0};
            Vector3d uGradient = {0, 0, 0};
            double tGradient = 0;
            fold = fnew;

            for(unsigned int i = 0; i < m; i++){

                Vector3d g = points[i] - p0 - u * t;
                p0Gradient += g;
                uGradient += g * t;
                tGradient += g.dot(u);

            }

            p0Gradient /= -m;
            uGradient /= -m;
            tGradient /= -m;
            p0 -= alpha * p0Gradient;
            u -= alpha * uGradient;
            t -= alpha * tGradient;

            fnew = 0;

            for(unsigned int i = 0; i < points.size(); i++)
                fnew += pow((points[i] - p0 - u * t).norm(), 2);

            fnew /= 2 * m;
            u /= u.norm();

        }while(fold > fnew + 10E-20);

        return std::make_pair(u, p0);

    }

    std::pair<Vector3d, Vector3d> linearRegression1(std::vector<Vector3d> points){

        int m = points.size();
        Vector3d p0 = points[0];
        p0(1)=0;
        Vector3d u = {0, 1, 0};

        double alpha = 0.005;
        double fold = DBL_MAX;
        double fnew = DBL_MAX;
        do{

            Vector3d p0Gradient = {0, 0, 0};
            Vector3d uGradient = {0, 0, 0};
            fold = fnew;

            for(unsigned int i = 0; i < m; i++){

                double t = (points[i](0) - p0(0)) * u(0) + (points[i](1) - p0(1)) * u(1) + (points[i](2) - p0(2)) * u(2);
                Vector3d g = points[i] - p0 - u * t;
                p0Gradient += g;
                uGradient += g * t;

            }

            p0Gradient /= -m;
            uGradient /= -m;
            p0 -= alpha * p0Gradient;
            u -= alpha * uGradient;

            fnew = 0;

            for(unsigned int i = 0; i < points.size(); i++){
                double t = (points[i](0) - p0(0)) * u(0) + (points[i](1) - p0(1)) * u(1) + (points[i](2) - p0(2)) * u(2);
                fnew += pow((points[i] - p0 - u * t).norm(), 2);
            }

            fnew /= 2 * m;
            u /= u(1);
            p0(1) = 0;

        }while(fold > fnew + 10E-20);

        return std::make_pair(u, p0);

    }


    std::vector<double *> imatiToDoublePointRepresentation(vector<Vertex*> points){

        vector<double*> newVector;

        for(unsigned int i = 0; i < points.size(); i++){
            double* p = malloc(2 * sizeof(double));
            p[0] = points[i]->x;
            p[1] = points[i]->z;
            newVector.push_back(p);
        }

        return newVector;
    }

    std::vector<Vector2d> imatiToEigen2DPointRepresentation(std::vector<Point> points){

        vector<Vector2d> newVector;

        for(unsigned int i = 0; i < points.size(); i++){
            Vector2d p = {points[i].x, p(1) = points[i].z};
            newVector.push_back(p);
        }

        return newVector;
    }

    std::vector<Vector3d> imatiToEigen3DPointRepresentation(std::vector<Point> points)
    {
        vector<Vector3d> newVector;

        for(unsigned int i = 0; i < points.size(); i++){
            Vector3d p = {points[i].x, points[i].y, points[i].z};
            newVector.push_back(p);
        }

        return newVector;
    }


    std::pair<Vector2d, Vector2d> planarPCA(std::vector<Vector2d> points){

        Vector2d mean;
        for(unsigned int i = 0; i < points.size(); i++)
            mean += points[i];

        mean /= points.size();

        Matrix2d sigma;
        sigma(0,0) = 0;
        sigma(0,1) = 0;
        sigma(1,0) = 0;
        sigma(1,1) = 0;

        for(unsigned int i = 0; i < points.size(); i++){
            sigma += (points[i] - mean) * ((points[i] - mean).transpose());
        }

        sigma /= points.size();
        JacobiSVD<MatrixXd> svd(sigma, ComputeFullU | ComputeFullV);
        MatrixXd U = svd.matrixU();
        Vector2d p1 = {U(0,0), U(0,1)};
        Vector2d p2 = {U(1,0), U(1,1)};


        return std::make_pair(p1, p2);

    }

    Eigen::Matrix3d spatialPCA(std::vector<Eigen::Vector3d> points){

        Vector3d mean;
        for(unsigned int i = 0; i < points.size(); i++)
            mean += points[i];

        mean /= points.size();

        Matrix3d sigma;
        sigma << 0, 0, 0,
                 0, 0, 0,
                 0, 0, 0;

        for(unsigned int i = 0; i < points.size(); i++){
            sigma += (points[i] - mean) * ((points[i] - mean).transpose());
        }

        sigma /= points.size();
        JacobiSVD<MatrixXd> svd(sigma, ComputeFullU | ComputeFullV);
        Matrix3d U = svd.matrixU();
        return U;
    }


    std::vector<double> getOBB(DrawableMesh* mesh){

        vector<vec> pointsVector;
        vector<double> bBPoints;
        List* V = &(mesh->V);
        IMATI_STL::Node* n;
        Vertex* v;
        FOREACHVVVERTEX(V, v, n){
            vec p = POINT_VEC(v->x, v->y, v->z);
            pointsVector.push_back(p);
        }

        vec* pv = pointsVector.data();
        OBB minOBB = OBB::OptimalEnclosingOBB(pv, pointsVector.size());

        for(unsigned int i = 0; i < 8; i++){
            bBPoints.push_back(minOBB.CornerPoint(i).x);
            bBPoints.push_back(minOBB.CornerPoint(i).y);
            bBPoints.push_back(minOBB.CornerPoint(i).z);
        }

        return bBPoints;
    }


    bool isALessThanB(std::pair<std::pair<double, double>, unsigned int> a, std::pair<std::pair<double, double>, unsigned int> b){

        if(a.first.first < b.first.first)
            return true;
        else if(a.first.first == b.first.first && a.first.second < b.first.second)
            return true;
        return false;

    }

    bool isLeft(Vector2d p, Vector2d p1, Vector2d p2){
        return (p(0) - p1(0)) * (p2(1) - p1(1)) - (p(1) - p1(1)) * (p2(0) - p1(0)) < 0;
    }

    std::vector<Vector2d> extractConvexHull(std::vector<Vector2d> points){

        //This function is based on the Graham's Algorithm
        unsigned int lowestRighmostPosition;
        double smallestY = DBL_MAX, biggestX = DBL_MAX;
        points.erase(points.end());

        vector<pair<pair<double, double>, unsigned int> > segmentsAngleAndLength;
        for(unsigned int i = 0; i < points.size(); i++)
            if(points[i](1) < smallestY){
                lowestRighmostPosition = i;
                smallestY = points[i](1);
            }else if(points[i](1) == smallestY && points[i](0) > biggestX){
                lowestRighmostPosition = i;
                biggestX = points[i](0);
            }

        Vector2d xAxis = {1, 0};
        for(unsigned int i = 0; i < points.size(); i++){

            if(i == lowestRighmostPosition)
                continue;

            Vector2d pi = points[i];
            Vector2d v = pi - points[lowestRighmostPosition];
            v /= v.norm();
            double angle = computeAngleBetweenVectors(xAxis, v);

            pair<double, double> angleAndLength = make_pair(angle, v.norm());
            pair<pair<double, double>, unsigned int> segmentAngleAndLength = make_pair(angleAndLength, i);
            segmentsAngleAndLength.push_back(segmentAngleAndLength);
        }

        sort(segmentsAngleAndLength.begin(), segmentsAngleAndLength.end(), isALessThanB);
        vector<Vector2d> convexHull;
        convexHull.push_back(points[lowestRighmostPosition]);
        convexHull.push_back(points[segmentsAngleAndLength[0].second]);

        unsigned int i = 2;
        while(i < segmentsAngleAndLength.size()){

            unsigned int actualPointPosition = segmentsAngleAndLength[i].second;
            Vector2d p = points[actualPointPosition];
            Vector2d p1 = convexHull.at(convexHull.size() - 2);
            Vector2d p2 = convexHull.back();
            if(isLeft(p, p1, p2)){
                convexHull.push_back(p);
                i++;
            }else{
                convexHull.pop_back();
            }

        }

        points.push_back(points.front());

        return convexHull;

    }

    std::vector<Vector2d> get2DOBB(std::vector<Vector2d> points){

        vector<Eigen::Vector2d> ch = Utilities::extractConvexHull(points);

        //This function is based on the Rotating Calipers Algorithm
        double minX = DBL_MAX, maxX = -DBL_MAX, minY = DBL_MAX, maxY = -DBL_MAX;
        vector<Vector2d> pivots = {{0,0}, {0,0}, {0,0}, {0,0}};
        vector<unsigned int> pivotPositions = {-1, -1, -1, -1};

        for(unsigned int i = 0; i < ch.size(); i++){
            if(ch[i](1) < minY){
                minY = ch[i](1);
                pivotPositions[0] = i;
                pivots[0] = ch[i];
            }
            if(ch[i](0) > maxX){
                maxX = ch[i](0);
                pivotPositions[1] = i;
                pivots[1] = ch[i];
            }
            if(ch[i](1) > maxY){
                maxY = ch[i](1);
                pivotPositions[2] = i;
                pivots[2] = ch[i];
            }
            if(ch[i](0) < minX){
                minX = ch[i](0);
                pivotPositions[3] = i;
                pivots[3] = ch[i];
            }
        }

        vector<Vector2d> bbs = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}};

        double area = (maxX - minX) * (maxY - minY);

        vector<Vector2d> rv{{maxX, minY}, {maxX, maxY}, {minX, maxY}, {minX, minY}};
        double performedRotation = 0;

        for(unsigned j = 0; j < ch.size() - 1; j++){

            vector<unsigned int> nextPositions;
            vector<Vector2d> vs;
            vector<Vector2d> intersections;
            double minAngle = DBL_MAX;
            unsigned int minAnglePivot;

            for(unsigned int i = 0; i < 4; i++){
                pivots[i] = ch[pivotPositions[i]];
                nextPositions.push_back((pivotPositions[i] + 1) % ch.size());
                vs.push_back(ch[nextPositions[i]] - pivots[i]);
                vs[i] /= vs[i].norm();
                double angle = computeAngleBetweenVectors(bbs[i], vs[i]);
                if(angle < minAngle){
                    minAngle = angle;
                    minAnglePivot = i;
                }
            }

            Matrix2d rotationMatrix = create2DRotationMatrix(minAngle);

            for(unsigned int i = 0; i < 4; i++)
                bbs[i] = rotationMatrix * bbs[i];

            for(unsigned int i = 0; i < 4; i++)
                intersections.push_back(computeLineIntersection(bbs[i], pivots[i], bbs[(i + 1) % 4], pivots[(i + 1) % 4]));

            double newArea = (intersections[1] - intersections[0]).norm() *
                             (intersections[2] - intersections[1]).norm();

            if(newArea < area){
                area = newArea;
                for(unsigned int i = 0; i < 4; i++)
                    rv[i] = intersections[i];
            }

            pivotPositions[minAnglePivot] = nextPositions[minAnglePivot];
            performedRotation += minAngle;


        };


        return rv;
    }


    double computeAngleBetweenVectors(Vector2d v1, Vector2d v2){

        double angle = acos(v1.dot(v2) / (v1.norm() * v2.norm()));
        return angle;

    }

    Matrix2d create2DRotationMatrix(double angle){

        Matrix2d rotationMatrix;
        rotationMatrix << cos(angle), -sin(angle),
                          sin(angle), cos(angle);

        return rotationMatrix;

    }

    Vector2d computeLineIntersection(Vector2d v1, Vector2d p1, Vector2d v2, Vector2d p2){

        double x;
        double a, b, c, d;
        bool aUndefined = false, bUndefined = false;

        if(v1(0) == 0){

            x = p1(0);
            aUndefined = true;

        }else{

            a = v1(1) / v1(0);
            c = (-a) * p1(0) + p1(1);

        }

        if(v2(0) == 0){

            x = p2(0);
            bUndefined = true;

        }else{

            b = v2(1) / v2(0);
            d = (-b) * p2(0) + p2(1);

        }

        if(!(aUndefined || bUndefined))
            x = (d - c) / (a - b);

        double y;

        if(aUndefined)
            y = b * x + d;
        else
            y = a * x + c;

        Vector2d intersection = {x, y};
        return intersection;
    }

    std::vector<Point> transformVertexList(std::vector<IMATI_STL::Vertex *> list, Matrix4d transformationMatrix){

        vector<Point> transformedPoints;
        for(unsigned int i = 0; i < list.size(); i++){
            Vector4d p = {list[i]->x, list[i]->y, list[i]->z, 1};
            Vector4d tr = transformationMatrix * p;
            Point tp(tr(0), tr(1), tr(2));
            transformedPoints.push_back(tp);
        }

        return transformedPoints;

    }
}
