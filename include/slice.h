#ifndef SLICE_H
#define SLICE_H

#include <vector>
#include <malloc.h>
#include <vtkSmartPointer.h>
#include <vtkContourTriangulator.h>
#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkLine.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkPolyDataMapper.h>
#include <vtkAssembly.h>
#include <vtkLine.h>
#include <vtkCellArray.h>
#include "trianglehelper.h"
#include "drawablemesh.h"
#include "manode.h"
#include "utilities.h"

class Slice
{
public:
    Slice();


    void addHole(const std::vector<IMATI_STL::Point> hole);
    void triangulate();

    DrawableMesh *getSlice() const;

    AndreasStructures::MANode *getSkeleton() const;
    void setSkeleton(AndreasStructures::MANode *value);
    void printInformation();

    vector<Point> getBoundingBox() const;
    vector<Point> getConvexHull() const;
    pair<Point, Point> getMinDiagonal() const;
    pair<Point, Point> getMaxDiagonal() const;
    vector<pair<Point, double> > getBestFittedCircles() const;


    Eigen::Matrix4d getFromPlaneTransformation() const;
    void setFromPlaneTransformation(const Eigen::Matrix4d &value);

    std::vector<IMATI_STL::Point> getBoundary() const;
    void setBoundary(const std::vector<IMATI_STL::Point> &value);

    std::vector<std::vector<IMATI_STL::Point> > getHoles() const;
    void setHoles(const std::vector<std::vector<IMATI_STL::Point> > &value);

private:

    DrawableMesh* slice;
    std::vector<IMATI_STL::Point> boundary;
    std::vector<std::vector<IMATI_STL::Point> > holes;
    AndreasStructures::MANode* skeleton;
    vector<Point> boundingBox;
    vector<Point> convexHull;
    vector<pair<Point, double> > bestFittedCircles;
    pair<Point, Point> minDiagonal;
    pair<Point, Point> maxDiagonal;
    Eigen::Matrix4d fromPlaneTransformation;

};

#endif // SLICE_H
