#ifndef TREENODE_H
#define TREENODE_H
#include <vector>
#include <algorithm>
#include "node.h"

namespace AndreasStructures {

    class TreeNode : public Node {
        public:
            TreeNode();

            TreeNode *getFather() const;
            void setFather(TreeNode *value);

            std::vector<TreeNode *> getSons() const;
            void setSons(const std::vector<TreeNode *> &value);
            void addSon(TreeNode * value);
            bool removeSon(TreeNode* son);

            TreeNode* getRoot();
            void removeSonsCycles();

            std::vector<TreeNode*> preOrderVisit();
            std::vector<TreeNode*> postOrderVisit();

        protected:
            TreeNode* father;
            std::vector<TreeNode*> sons;

    };
}


#endif // TREENODE_H
