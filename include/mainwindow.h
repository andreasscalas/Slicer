#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QFileDialog>
#include <QVTKInteractor.h>
#include <qdebug.h>
#include <vtkSmartPointer.h>
#include <vtkCellArray.h>
#include <vtkCell.h>
#include <vtkLine.h>
#include <vtkPolygon.h>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkVectorText.h>
#include <vtkCamera.h>
#include <vtkInteractorStyleImage.h>
#include <vtkGeometryFilter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPLYWriter.h>
#include <vtkRegularPolygonSource.h>
#include <vtkDoubleArray.h>
#include <vtkMatrix4x4.h>
#include <vtkOutlineSource.h>
#include <math.h>
#include "imatistl.h"
#include "drawablemesh.h"
#include "utilities.h"
#include "contourcontainmenttreenode.h"
#include "manode.h"
#include "slicer.h"
#include <memory>
#include "slice.h"
#include <MathGeoLib/MathGeoLib.h>
#include <MathGeoLib/MathBuildConfig.h>
#include <MathGeoLib/Math/myassert.h>
#include <MathGeoLib/Algorithm/GJK.h>
#include <MathGeoLib/Algorithm/SAT.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    const std::string initial_instruction = "To open a mesh file, click on the folder button above";

private slots:
    void slotOpenFile();

    void slotClear();

    void on_xSlider_valueChanged(int value);

    void on_ySlider_valueChanged(int value);

    void on_zSlider_valueChanged(int value);

    void on_showNodesBox_stateChanged(int arg1);

    void on_showArcNodesBox_stateChanged(int arg1);

    void on_maxErrorSlider_valueChanged(int value);

    void on_showSlices_stateChanged(int arg1);

    void on_showFittedCircle_stateChanged(int arg1);

    void on_showSkeleton_stateChanged(int arg1);

    void on_showConvexHull_stateChanged(int arg1);

    void on_showboundingBox_stateChanged(int arg1);

    void on_showDiagonals_stateChanged(int arg1);

    void on_xSlider2_valueChanged(int value);

    void on_ySlider2_valueChanged(int value);

    void on_zSlider2_valueChanged(int value);

    void on_showBoundingBox_stateChanged(int arg1);

    void on_showCenters_stateChanged(int arg1);

private:
    Ui::MainWindow *ui;
    DrawableMesh* model;
    vtkSmartPointer<vtkRenderer>  ren;
    vtkSmartPointer<vtkRenderer>  sliceRen;
    vtkSmartPointer<vtkAssembly>  meshAssembly;
    vtkSmartPointer<vtkAssembly>  sliceAssembly;
    vtkSmartPointer<vtkActor>  meshActor;
    vtkSmartPointer<vtkActor>  writeActor;
    QVTKInteractor *iren;
    Eigen::Matrix4d xRotationMatrix;
    Eigen::Matrix4d yRotationMatrix;
    Eigen::Matrix4d zRotationMatrix;
    Eigen::Vector3d translationVector;
    Eigen::Matrix4d translationMatrix;
    std::vector<double> boundingBox;
    Point normal;
    Point center;
    int previousXValue;
    int previousYValue;
    int previousZValue;
    int previousHeightValue;
    double totalWidth;
    double totalHeight;
    double totalDepth;
    double shift;
    double allowedError;
    const static double MAX_ERROR = 0.05;
    bool showSliceOnMesh;
    bool showBoundingBox;
    bool showSkeleton;
    bool showNodes;
    bool showArcNodes;
    bool showFittedCircle;
    bool showConvexHull;
    bool showBoundingRectangle;
    bool showDiagonals;
    bool showCenters;
    Slicer* slicer;

    const double BORDEAUX[3] = {156.0/255.0, 14.0/255.0, 14.0/255.0};
    const double SLICE_COLOR[3] = {0, 51/255, 0};

protected:
    void init();
    void clear();
    void write(std::string message);
    void updateView();
    void updateIntersection();
};

#endif // MAINWINDOW_H
