#ifndef UTILITIES_H
#define UTILITIES_H

#include <vector>
#include <queue>
#include <stack>
#include <map>
#include <eigen3/Eigen/LU>
#include <eigen3/Eigen/Dense>
#include <math.h>
#include <vtkSmartPointer.h>
#include <vtkPlane.h>
#include "imatistl.h"
#include "drawablemesh.h"
#include "manode.h"
#include "maarcpath.h"

class DrawableMesh;

namespace Utilities {

    const double EPSILON = 1E-5;
    const double STEP_SIZE = 0.1;

    Eigen::Matrix4d xRotationMatrix(double alfa);
    Eigen::Matrix4d yRotationMatrix(double beta);
    Eigen::Matrix4d zRotationMatrix(double gamma);
    Eigen::Matrix4d translationMatrix(Eigen::Vector3d direction);

    std::vector<IMATI_STL::Point> transformVertexList(std::vector<IMATI_STL::Vertex*> list, Eigen::Matrix4d transformationMatrix);

    /**
     * @brief mod method for extracting the modulo operator
     * @param val the value on which the modulo is applied
     * @param m modulus of the operation
     * @return the congruence value
     */
    int mod(int val, int m);

    /**
     * @brief isPointInsidePolygon is a method for checking if a point (v) is inside a polygon, defined as an ordered set of vertices (boundary)
     * @param v The point
     * @param boundary The polygon
     * @return true if the point is inside the polygon, false otherwise
     */
    bool isPointInsidePolygon(IMATI_STL::Point v, std::vector<IMATI_STL::Point> boundary);

    /**
     * @brief isBoundaryClockwise is a method for checking the order (clockwise or counterclockwise) of a boundary, defined as an ordered set of vertices
     * @param boundary the boundary to check
     * @return true if the boundary is clockwise, false otherwise
     */
    bool isBoundaryClockwise(std::vector<IMATI_STL::Point> boundary);

    /**
     * @brief rotateVector is a method for applying a trasformation (rotation) to a vector, given a appropriate transform matrix.
     * @param v the vector to transform
     * @return the transformed vector
     */
    IMATI_STL::Point rotateVector(IMATI_STL::Point v, Eigen::Matrix3d);

    /**
     * @brief countBoundaryEdges method for counting the number of edges which are on the boundary of the mesh.
     * @param t the triangle on which the number of boundary edges should be counted.
     * @return the number of boundary edges.
     */
    short countBoundaryEdges(IMATI_STL::Triangle* t);

    /**
     * @brief getBoundaryEdges method for extracting the edges which are on the boundary of the mesh from a triangle
     * @param t the triangle from which extracting the boundary edges
     * @return the boundary edges
     */
    std::vector<Edge*> getBoundaryEdges(IMATI_STL::Triangle *t);

    /**
     * @brief getMAStartingTriangle is a method for obtaining the better triangle with which starting the Medial Axis Transform algorithm.
     * @param slice the mesh from which extracting the triangle
     * @return
     */
    IMATI_STL::Triangle* getMAStartingTriangle(DrawableMesh* slice);

    /**
     * @brief walkBranch method for tackling the management of each branch of the Medial Axis Transform
     * @param startingEdge the edge from which the branch is starting
     * @param startingTriangle the triangle from which the branch is starting
     * @param first the starting node
     * @param path pointer to the path that is being created
     * @return the obtained node (can be either a closing node - obtained from a 2-constrained triangle - or a new branching)
     */
    AndreasStructures::MANode* walkBranch(IMATI_STL::Edge* startingEdge, IMATI_STL::Triangle* startingTriangle, AndreasStructures::Node* first, std::vector<IMATI_STL::Point* >* path);

    /**
     * @brief medialAxisTransform method for obtaining the Medial Axis Transform from a given mesh.
     * See "Similarity measures for blending polygonal shapes" by Mortara and Spagnuolo.
     * @param mesh the mesh from which the MAT will be extracted.
     * @return the obtained MAT (which is a skeleton - a graph ).
     */
    AndreasStructures::MANode* medialAxisTransform(DrawableMesh* mesh);

    template <typename T>
    std::vector<std::pair<T, T> > findPair(std::vector<std::pair<T, T> > list, std::pair<T, T> el){
        typename std::vector<std::pair<T, T> >::iterator it;
        for(it = list.begin(); it != list.end(); it++){
            if((it->first == el.first && it->second == el.second) ||
               (it->first == el.second && it->second == el.first))
                    break;
        }
        return it;
    }

    /**
     * @brief simplifyLine method for simplifying a line, defined in "Algorithms for the reduction of the Number of Points Required to Represent a Digitized Line or its Caricature
     * @param line the line to simplify
     * @param maxError the maximum error allowed for each approximation
     * @return the simplified line
     */
    std::vector<IMATI_STL::Vertex*> simplifyLine(std::vector<IMATI_STL::Vertex*> line, double maxError);

    std::pair<double*, double> circleFitting(std::vector<double*> points);

    std::pair<Eigen::Vector3d, Eigen::Vector3d> linearRegression(std::vector<Eigen::Vector3d> points);
    std::pair<Eigen::Vector3d, Eigen::Vector3d> linearRegression1(std::vector<Eigen::Vector3d> points);

    std::pair<Eigen::Vector2d, double> circleFitting(std::vector<Eigen::Vector2d> points);

    std::pair<Eigen::Vector2d, Eigen::Vector2d> planarPCA(std::vector<Eigen::Vector2d> points);
    Eigen::Matrix3d spatialPCA(std::vector<Eigen::Vector3d> points);

    std::vector<double*> imatiToDoublePointRepresentation(std::vector<IMATI_STL::Vertex*> points);

    std::vector<Eigen::Vector2d> imatiToEigen2DPointRepresentation(std::vector<IMATI_STL::Point> points);

    std::vector<Eigen::Vector3d> imatiToEigen3DPointRepresentation(std::vector<IMATI_STL::Point> points);

    std::vector<Eigen::Vector2d> extractConvexHull(std::vector<Eigen::Vector2d> points);

    bool isLeft(Eigen::Vector2d p, Eigen::Vector2d p1, Eigen::Vector2d p2);

    bool isALessThanB(std::pair<std::pair<double, double>, unsigned int> a, std::pair<std::pair<double, double>, unsigned int> b);

    double computeAngleBetweenVectors(Eigen::Vector2d v1, Eigen::Vector2d v2);
    Eigen::Matrix3d create2DTranslationMatrix(Eigen::Vector2d direction);
    Eigen::Matrix2d create2DRotationMatrix(double angle);
    Eigen::Vector2d computeLineIntersection(Eigen::Vector2d v1, Eigen::Vector2d p1, Eigen::Vector2d v2, Eigen::Vector2d p2);
    vector<Eigen::Vector2d> get2DOBB(std::vector<Eigen::Vector2d> points);
    std::vector<double> getOBB(DrawableMesh* mesh);

}
#endif // UTILITIES_H
