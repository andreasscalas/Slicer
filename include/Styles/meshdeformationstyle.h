#ifndef MESHDEFORMATIONSTYLE_H
#define MESHDEFORMATIONSTYLE_H

#include <vtkSmartPointer.h>
#include <vtkPointPicker.h>
#include <vtkActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkObjectFactory.h>
#include <vtkCoordinate.h>
#include <vtkCamera.h>
#include <QVTKWidget.h>
#include "drawablemesh.h"
#include "barycentriccoordinates.h"

#define VTK_CREATE(type, name) \
    vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

/**
 * @brief The MeshDeformationStyle class controls the interaction with the points of a cage
 */
class MeshDeformationStyle : public vtkInteractorStyleTrackballCamera{

    public:
        vtkSmartPointer<vtkPolyData> data;                  //Linking between coordinates and ids
        vtkSmartPointer<vtkPointPicker> pointPicker;        //The point picker
        vtkSmartPointer<vtkAssembly> assembly;              //Assembly of actors
        QVTKWidget* qvtkwidget;
        DrawableMesh* model;
        DrawableMesh* cage;                                 //The cage
        BarycentricCoordinates* coords;
        int i = 0;

        bool Move;                                          //True if some point has been picked
        std::vector<long int>* SelectedPoints;              //Id of the picked point
        double clickPos[3];

        static MeshDeformationStyle* New();

        MeshDeformationStyle(){
            this->Move = false;
            this->pointPicker = vtkSmartPointer<vtkPointPicker>::New();
        }

        vtkTypeMacro(MeshDeformationStyle, vtkInteractorStyleTrackballCamera)

        void OnMouseMove(){

            if(this->Interactor->GetControlKey() && Move){

                /* While the mouse is moved on the display, the point position is updated
                 * as the point obtained projecting the point selected in the camera plane
                 * on the dragging plane */
                int mousePosition[2];
                double position[4];
                this->Interactor->GetEventPosition(mousePosition);
                this->ComputeDisplayToWorld(mousePosition[0], mousePosition[1], 0, position);
                double shift[3] = {position[0] - clickPos[0], position[1] - clickPos[1], position[2] - clickPos[2]};

                for(int i = 0; i < this->SelectedPoints->size(); i++){
                    double *oldPosition = this->data->GetPoint((*SelectedPoints)[i]);
                    double newPosition[3] = {oldPosition[0] + shift[0], oldPosition[1] + shift[1], oldPosition[2] + shift[2]};
                    this->data->GetPoints()->SetPoint((*SelectedPoints)[i], newPosition);
                    this->cage->setPointPosition((*SelectedPoints)[i], newPosition);
                }
                this->data->Modified();
                coords->deform();
                this->GetCurrentRenderer()->RemoveActor(assembly);
                this->model->setMeshModified(true);
                this->model->update();
                this->assembly->Modified();
                this->GetCurrentRenderer()->AddActor(assembly);
                qvtkwidget->update();
                clickPos[0] = position[0];
                clickPos[1] = position[1];
                clickPos[2] = position[2];

            }else if(Move) //The interaction is stopped
                this->Move = false;

            vtkInteractorStyleTrackballCamera::OnMouseMove();

        }

        void OnRightButtonUp(){

            //The interaction is stopped
            if(this->Interactor->GetControlKey() && Move==true)
                this->Move = false;


            vtkInteractorStyleTrackballCamera::OnRightButtonUp();

        }

        void OnRightButtonDown(){

            //If the user is trying to pick a point...
            if(this->Interactor->GetControlKey()){
                int mousePosition[2];
                double position[4];

                this->Interactor->GetEventPosition(mousePosition);
                this->FindPokedRenderer(mousePosition[0], mousePosition[1]);
                this->ComputeDisplayToWorld(mousePosition[0], mousePosition[1], 0, position);
                clickPos[0] = position[0];
                clickPos[1] = position[1];
                clickPos[2] = position[2];

                this->Move = true;

                if(SelectedPoints->size() == 0){

                    //Some tolerance is set for the picking
                    this->pointPicker->SetTolerance(0.01);
                    this->pointPicker->Pick(mousePosition[0], mousePosition[1], 0, this->GetCurrentRenderer());
                    //If some point has been picked...
                    if(this->pointPicker->GetPointId() > 0 && this->pointPicker->GetPointId() < this->cage->V.numels()){

                        this->SelectedPoints->push_back(this->pointPicker->GetPointId());
                        cage->setSelectedPoints(*SelectedPoints);

                    }
                }
            }else
                vtkInteractorStyleTrackballCamera::OnRightButtonDown();
        }

        QVTKWidget *MeshDeformationStyle::getQvtkwidget() const
        {
            return qvtkwidget;
        }

        void MeshDeformationStyle::setQvtkwidget(QVTKWidget *value)
        {
            qvtkwidget = value;
        }

        vtkSmartPointer<vtkAssembly> MeshDeformationStyle::getAssembly() const
        {
        return assembly;
        }

        void MeshDeformationStyle::setAssembly(const vtkSmartPointer<vtkAssembly> &value)
        {
        assembly = value;
        }

        DrawableMesh *MeshDeformationStyle::getModel() const
        {
        return model;
        }

        void MeshDeformationStyle::setModel(DrawableMesh *value)
        {
        model = value;
        }

        DrawableMesh *MeshDeformationStyle::getCage() const
        {
        return cage;
        }

        void MeshDeformationStyle::setCage(DrawableMesh *value)
        {
        cage = value;
        }

        BarycentricCoordinates *MeshDeformationStyle::getCoords() const
        {
        return coords;
        }

        void MeshDeformationStyle::setCoords(BarycentricCoordinates *value)
        {
        coords = value;
        }

        std::vector<long> *MeshDeformationStyle::getSelectedPoints() const
        {
        return SelectedPoints;
        }

        void MeshDeformationStyle::setSelectedPoints(std::vector<long> *value)
        {
        SelectedPoints = value;
        }

        vtkSmartPointer<vtkPolyData> MeshDeformationStyle::getData() const
        {
            return data;
        }

        void MeshDeformationStyle::setData(const vtkSmartPointer<vtkPolyData> &value)
        {
            data = value;
        }
};
#endif // MESHDEFORMATIONSTYLE_H


