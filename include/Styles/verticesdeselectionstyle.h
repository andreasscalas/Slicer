#ifndef MESHDEFORMATIONSTYLE_H
#define MESHDEFORMATIONSTYLE_H

#include <vtkSmartPointer.h>
#include <vtkPointPicker.h>
#include <vtkActor.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkObjectFactory.h>
#include <vtkCoordinate.h>
#include <vtkCamera.h>
#include <vtkPlane.h>
#include "drawablemesh.h"

/**
 * @brief The MeshDeformationStyle class controls the interaction with the points of a cage
 */
class MeshDeformationStyle : public vtkInteractorStyleTrackballCamera{

    public:
        vtkSmartPointer<vtkPolyData> data;              //Linking between coordinates and ids
        vtkSmartPointer<vtkPointPicker> pointPicker;    //The point picker
        vtkSmartPointer<vtkAssembly> assembly;          //Assembly of actors
        vtkSmartPointer<vtkPlane> draggingPlane;        //The plane on which the point will be dragged
        DrawableMesh* model;
        DrawableMesh* cage;                             //The cage
        BarycentricCoordinates* coords;

        bool Move;                                      //true if some point has been picked
        vtkIdType SelectedPoint;                        //Id of the picked point

    public:

        static MeshDeformationStyle* New();

        MeshDeformationStyle(){
            this->Move = false;
            this->pointPicker = vtkSmartPointer<vtkPointPicker>::New();
            this->draggingPlane = vtkSmartPointer<vtkPlane>::New();

        }

        vtkTypeMacro(MeshDeformationStyle, vtkInteractorStyleTrackballCamera)

        void OnMouseWheelForward(){

            this->FindPokedRenderer(this->Interactor->GetEventPosition()[0],
                                      this->Interactor->GetEventPosition()[1]);
              if (this->CurrentRenderer == NULL)
                return;

              this->GrabFocus((vtkCommand*)this->EventCallbackCommand);
              this->StartZoom();
              this->GetCurrentRenderer()->GetActiveCamera()->Zoom(1.2);
              this->EndZoom();
              this->ReleaseFocus();

        }

        void OnMouseWheelBackward(){

            this->FindPokedRenderer(this->Interactor->GetEventPosition()[0],
                                      this->Interactor->GetEventPosition()[1]);
              if (this->CurrentRenderer == NULL)
              {
                return;
              }

              this->GrabFocus((vtkCommand*)this->EventCallbackCommand);
              this->StartZoom();
              this->GetCurrentRenderer()->GetActiveCamera()->Zoom(0.8);
              this->EndZoom();
              this->ReleaseFocus();

        }

        void OnMouseMove(){

            if(this->Interactor->GetControlKey() && Move){

                /* While the mouse is moved on the display, the point position is updated
                 * as the point obtained projecting the point selected in the caera plane
                 * on the dragging plane */
                int* mousePosition = this->Interactor->GetEventPosition();
                vtkSmartPointer<vtkCoordinate> coordinates = vtkSmartPointer<vtkCoordinate>::New();
                coordinates->SetCoordinateSystemToDisplay();
                coordinates->SetValue(mousePosition[0], mousePosition[1]);
                double *p = coordinates->GetComputedWorldValue(this->GetCurrentRenderer());
                double pp[3];
                draggingPlane->ProjectPoint(p,pp);
                this->data->GetPoints()->SetPoint(this->SelectedPoint, pp);
                this->data->Modified();
                this->cage->setPointPosition(this->SelectedPoint, pp);
                this->GetCurrentRenderer()->RemoveActor(assembly);
                this->cage->draw(assembly);
                coords->deform();
                this->model->update();
                this->GetCurrentRenderer()->RemoveActor(assembly);
                this->model->draw(assembly);
                this->GetCurrentRenderer()->AddActor(assembly);
                this->GetCurrentRenderer()->AddActor(assembly);
                this->GetCurrentRenderer()->Render();
                this->GetCurrentRenderer()->GetRenderWindow()->Render();

            }else if(Move)//The interaction is stopped
                endMove();

                vtkInteractorStyleTrackballCamera::OnMouseMove();

        }

        void OnRightButtonUp(){

            //The interaction is stopped
            if(this->Interactor->GetControlKey() && Move==true)
                endMove();


            vtkInteractorStyleTrackballCamera::OnRightButtonUp();

        }

        virtual void OnRightButtonDown(){

            //If the user is trying to pick a point...
            if(this->Interactor->GetControlKey()){
                //The click position of the mouse is taken
                double x, y;
                x = this->Interactor->GetEventPosition()[0];
                y = this->Interactor->GetEventPosition()[1];
                this->FindPokedRenderer(x, y);
                //Some tolerance is set for the picking
                this->pointPicker->SetTolerance(0.01);
                this->pointPicker->Pick(x, y, 0, this->GetCurrentRenderer());

                //If some point has been picked...
                if(this->pointPicker->GetPointId() > 0 && this->pointPicker->GetPointId() < this->cage->V.numels()){

                    //Switch in move mode
                    this->Move = true;
                    this->SelectedPoint = this->pointPicker->GetPointId();

                    //The point position is obtained...
                    double p[3];
                    this->data->GetPoint(this->SelectedPoint, p);

                    //The camera position is taken...
                    double *c = this->GetCurrentRenderer()->GetActiveCamera()->GetPosition();

                    /* Creation of the dragging plane as the plane passing through the selected point and
                     * orthogonal to the line passing through the selected point and the camera position */
                    Point pp(p[0], p[1], p[2]);
                    Point cp(c[0], c[1], c[2]);
                    Point pc((cp-pp)/(cp.distance(pp)));
                    draggingPlane->SetNormal(pc.x,pc.y,pc.z);
                    draggingPlane->SetOrigin(p[0],p[1],p[2]);
                }
            }else
                vtkInteractorStyleTrackballCamera::OnRightButtonDown();
        }

        void endMove(){
            this->Move = false;
            this->SelectedPoint = -1;
            this->GetCurrentRenderer()->Render();
            this->GetCurrentRenderer()->GetRenderWindow()->Render();
        }

};
#endif // MESHDEFORMATIONSTYLE_H
