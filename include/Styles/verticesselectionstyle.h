#ifndef VERTICESSELECTIONSTYLE_H
#define VERTICESSELECTIONSTYLE_H

#include "drawablemesh.h"
#include "utilities.h"
#include <vtkSmartPointer.h>
#include <vtkPointPicker.h>
#include <vtkActor.h>
#include <vtkInteractorStyleRubberBandPick.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkObjectFactory.h>
#include <vtkCoordinate.h>
#include <vtkCamera.h>
#include <vtkDataSetMapper.h>
#include <vtkAreaPicker.h>
#include <vtkRenderedAreaPicker.h>
#include <vtkExtractGeometry.h>
#include <vtkSelectVisiblePoints.h>
#include <QVTKWidget.h>
#include <vector>

#define VTKISRBP_ORIENT 0
#define VTKISRBP_SELECT 1

/**
 * @brief The VerticesSelectionStyle class controls the interaction with the points of a mesh
 */
class VerticesSelectionStyle : public vtkInteractorStyleRubberBandPick{

    public:

        vtkSmartPointer<vtkAssembly> assembly;          //Assembly of actors
        vtkSmartPointer<vtkPolyData> Points;
        vtkSmartPointer<vtkActor> SelectedActor;
        vtkSmartPointer<vtkDataSetMapper> SelectedMapper;
        std::vector<long>* SelectedPoints;
        vtkSmartPointer<vtkPointPicker> pointPicker;        //The point picker
        QVTKWidget* qvtkwidget;
        bool selectionMode;
        bool visiblePointsOnly;
        DrawableMesh* model;

        static VerticesSelectionStyle* New();

        VerticesSelectionStyle(){

            selectionMode = true;
            visiblePointsOnly = true;
            this->SelectedMapper = vtkSmartPointer<vtkDataSetMapper>::New();
            this->SelectedActor = vtkSmartPointer<vtkActor>::New();
            this->SelectedActor->SetMapper(SelectedMapper);
            this->pointPicker = vtkSmartPointer<vtkPointPicker>::New();

        }

        vtkTypeMacro(VerticesSelectionStyle,vtkInteractorStyleRubberBandPick)

        virtual void OnRightButtonDown(){

            //If the user is trying to pick a point...
            //The click position of the mouse is taken
            double x, y;
            x = this->Interactor->GetEventPosition()[0];
            y = this->Interactor->GetEventPosition()[1];
            this->FindPokedRenderer(x, y);
            //Some tolerance is set for the picking
            this->pointPicker->SetTolerance(0.01);
            this->pointPicker->Pick(x, y, 0, this->GetCurrentRenderer());
            vtkIdType pointID = this->pointPicker->GetPointId();
            //If some point has been picked...
            if(pointID >= 0 && pointID < this->model->V.numels()){

                if(selectionMode)
                    this->SelectedPoints->push_back(pointID);
                else{
                    std::vector<long>::iterator it = std::find(SelectedPoints->begin(),SelectedPoints->end(), pointID);
                    if(it != SelectedPoints->end())
                        this->SelectedPoints->erase(it);
                }
                modifySelectedPoints();

            }
            vtkInteractorStyleRubberBandPick::OnRightButtonDown();

        }

        virtual void OnLeftButtonDown(){

            if(this->Interactor->GetControlKey()){
                this->CurrentMode = VTKISRBP_SELECT;

            }
            vtkInteractorStyleRubberBandPick::OnLeftButtonDown();

        }

        virtual void OnLeftButtonUp(){

            vtkInteractorStyleRubberBandPick::OnLeftButtonUp();
            if(this->CurrentMode==VTKISRBP_SELECT){

                this->CurrentMode = VTKISRBP_ORIENT;

                // Forward events

                vtkPlanes* frustum = static_cast<vtkRenderedAreaPicker*>(this->GetInteractor()->GetPicker())->GetFrustum();

                vtkSmartPointer<vtkExtractGeometry> extractGeometry = vtkSmartPointer<vtkExtractGeometry>::New();
                extractGeometry->SetImplicitFunction((vtkImplicitFunction*) frustum);

                #if VTK_MAJOR_VERSION <= 5
                    extractGeometry->SetInput(this->Points);
                #else
                    extractGeometry->SetInputData(this->Points);
                #endif
                    extractGeometry->Update();

                vtkSmartPointer<vtkVertexGlyphFilter> glyphFilter = vtkSmartPointer<vtkVertexGlyphFilter>::New();
                glyphFilter->SetInputConnection(extractGeometry->GetOutputPort());
                glyphFilter->Update();

                vtkSmartPointer<vtkPolyData> selected;
                if(visiblePointsOnly){
                    vtkSmartPointer<vtkSelectVisiblePoints> selectVisiblePoints = vtkSmartPointer<vtkSelectVisiblePoints>::New();
                    selectVisiblePoints->SetInputConnection(glyphFilter->GetOutputPort());
                    selectVisiblePoints->SetRenderer(this->GetCurrentRenderer());
                    selectVisiblePoints->Update();
                    selected = selectVisiblePoints->GetOutput();
                }else
                    selected = glyphFilter->GetOutput();

                #if VTK_MAJOR_VERSION <= 5
                    this->SelectedMapper->SetInput(selected);
                #else
                    this->SelectedMapper->SetInputData(selected);
                #endif
                    this->SelectedMapper->ScalarVisibilityOff();

                vtkSmartPointer<vtkIdTypeArray> ids = vtkIdTypeArray::SafeDownCast(selected->GetPointData()->GetArray("OriginalIds"));

                if(ids == nullptr)
                    return;

                for(vtkIdType i = 0; i < ids->GetNumberOfTuples(); i++){
                    std::vector<long>::iterator it = std::find(SelectedPoints->begin(), SelectedPoints->end(), ids->GetValue(i));
                    if(selectionMode){
                        if(it == SelectedPoints->end())
                            SelectedPoints->push_back(ids->GetValue(i));
                    }else{
                        if(it != SelectedPoints->end())
                            this->SelectedPoints->erase(it);
                    }

                }

                modifySelectedPoints();
            }

        }

        void modifySelectedPoints(){
            model->setSelectedPoints(*SelectedPoints);
            this->model->draw(assembly);
            this->qvtkwidget->update();
        }

        vtkSmartPointer<vtkPolyData> VerticesSelectionStyle::getPoints() const
        {
            return Points;
        }

        void VerticesSelectionStyle::setPoints(const vtkSmartPointer<vtkPolyData> &value)
        {
            Points = value;
        }

        vtkSmartPointer<vtkActor> VerticesSelectionStyle::getSelectedActor() const
        {
        return SelectedActor;
        }

        void VerticesSelectionStyle::setSelectedActor(const vtkSmartPointer<vtkActor> &value)
        {
        SelectedActor = value;
        }

        vtkSmartPointer<vtkDataSetMapper> VerticesSelectionStyle::getSelectedMapper() const
        {
        return SelectedMapper;
        }

        void VerticesSelectionStyle::setSelectedMapper(const vtkSmartPointer<vtkDataSetMapper> &value)
        {
        SelectedMapper = value;
        }

        std::vector<long> *VerticesSelectionStyle::getSelectedPoints() const
        {
        return SelectedPoints;
        }

        void VerticesSelectionStyle::setSelectedPoints(std::vector<long> *value)
        {
        SelectedPoints = value;
        }

        vtkSmartPointer<vtkPointPicker> VerticesSelectionStyle::getPointPicker() const
        {
        return pointPicker;
        }

        void VerticesSelectionStyle::setPointPicker(const vtkSmartPointer<vtkPointPicker> &value)
        {
        pointPicker = value;
        }

        bool VerticesSelectionStyle::getSelectionMode() const
        {
        return selectionMode;
        }

        void VerticesSelectionStyle::setSelectionMode(bool value)
        {
        selectionMode = value;
        }

        bool VerticesSelectionStyle::getVisiblePointsOnly() const
        {
        return visiblePointsOnly;
        }

        void VerticesSelectionStyle::setVisiblePointsOnly(bool value)
        {
        visiblePointsOnly = value;
        }

        DrawableMesh *VerticesSelectionStyle::getModel() const
        {
        return model;
        }

        void VerticesSelectionStyle::setModel(DrawableMesh *value)
        {
        model = value;
        }

        vtkSmartPointer<vtkAssembly> VerticesSelectionStyle::getAssembly() const
        {
            return assembly;
        }

        void VerticesSelectionStyle::setAssembly(const vtkSmartPointer<vtkAssembly> &value)
        {
            assembly = value;
        }

        QVTKWidget *VerticesSelectionStyle::getQvtkwidget() const
        {
            return qvtkwidget;
        }

        void VerticesSelectionStyle::setQvtkwidget(QVTKWidget *value)
        {
            qvtkwidget = value;
        }
};



#endif // VERTICESSELECTIONSTYLE_H




