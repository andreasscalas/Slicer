#ifndef GENERATIONSTYLE_H
#define GENERATIONSTYLE_H
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkSmartPointer.h>
#include <vtkPlaneSource.h>
#include <vtkPlane.h>
#include <vtkActor.h>
#include <vtkAssembly.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkCellArray.h>
#include <vtkCamera.h>
#include <vtkCoordinate.h>
#include <QVTKWidget.h>
#include <qslider.h>
#include <vector>
#include <qdebug.h>
#include "drawablemesh.h"
#include "eigen3/Eigen/Dense"
#include "utilities.h"
#include "ui_mainwindow.h"

#define VTK_CREATE(type, name) \
    vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

class GenerationStyle : public vtkInteractorStyleTrackballCamera{

    public:
        double originalCenter[3];
        double originalNormal[3];
        vtkSmartPointer<vtkPlane> slidingPlane;
        vtkSmartPointer<vtkPlaneSource> ps;
        vtkSmartPointer<vtkActor> planeActor;
        vtkSmartPointer<vtkActor> pointsActor;
        vtkSmartPointer<vtkAssembly> assembly;              //Assembly of actors
        vtkSmartPointer<vtkRenderer> ren;
        vtkSmartPointer<vtkPoints> cagePoints;
        vector<vtkIdType> pids;
        QVTKWidget* renderingWidget;
        QSlider* planeTranslationSlider;
        DrawableMesh* model;
        DrawableMesh* cage;
        double planeRange;
        bool showPlane;
        bool moving;

        static const int SLIDER_ORIGINAL_VALUE = 0;

        static GenerationStyle* New();

        GenerationStyle(){

            slidingPlane = vtkSmartPointer<vtkPlane>::New();
            ps = vtkSmartPointer<vtkPlaneSource>::New();
            planeActor = vtkSmartPointer<vtkActor>::New();
            pointsActor = vtkSmartPointer<vtkActor>::New();
            ren = vtkSmartPointer<vtkRenderer>::New();
            cage = new DrawableMesh();
            cagePoints = vtkSmartPointer<vtkPoints>::New();
            moving = false;
            showPlane = false;

        }

        vtkTypeMacro(GenerationStyle, vtkInteractorStyleTrackballCamera)

        void slidePlane(int position){

            double center[3];
            slidingPlane->GetOrigin(center);
            double adding = (((double) position) / 1000) * (planeRange - 0.01);
            double normal[3];
            this->slidingPlane->GetNormal(normal);
            slidingPlane->SetOrigin(originalCenter[0] + normal[0] * adding, originalCenter[1] + normal[1] * adding, originalCenter[2] + normal[2] * adding);
            assembly->RemovePart(planeActor);
            ps->SetCenter(slidingPlane->GetOrigin());
            ps->SetNormal(slidingPlane->GetNormal());
            ps->Update();
            vtkSmartPointer<vtkPolyData> plane = ps->GetOutput();
            vtkSmartPointer<vtkPolyDataMapper> pmapper = vtkSmartPointer<vtkPolyDataMapper>::New();
            #if VTK_MAJOR_VERSION <= 5
                pmapper->SetInput(plane);
            #else
                pmapper->SetInputData(plane);
            #endif
            planeActor->SetMapper(pmapper);
            planeActor->GetProperty()->SetOpacity(0.5);
            planeActor->GetProperty()->SetColor(0,1,0);
            assembly->AddPart(planeActor);
            assembly->Modified();

        }

        void initPlane(){

            double normal[3];
            Point center = model->getCenter();
            double bounds[6];
            assembly->GetBounds(bounds);
            assembly->RemovePart(planeActor);
            double xrange = abs(bounds[1]-bounds[0]);
            double yrange = abs(bounds[3]-bounds[2]);
            double zrange = abs(bounds[5]-bounds[4]);
            planeRange = std::max(xrange, yrange);
            planeRange = std::max(planeRange, zrange);
            ren->GetActiveCamera()->GetViewPlaneNormal(normal);
            originalCenter[0] = center.x;
            originalCenter[1] = center.y;
            originalCenter[2] = center.z;
            originalNormal[0] = normal[0];
            originalNormal[1] = normal[1];
            originalNormal[2] = normal[2];
            slidingPlane->SetOrigin(originalCenter);
            slidingPlane->SetNormal(originalNormal);
            ps->SetCenter(originalCenter);
            ps->SetNormal(originalNormal);
            ps->SetOrigin(-1000, -1000, 0);
            ps->SetPoint1(-1000, 1000, 0);
            ps->SetPoint2(1000, -1000, 0);
            ps->Update();
            vtkSmartPointer<vtkPolyData> plane = ps->GetOutput();
            vtkSmartPointer<vtkPolyDataMapper> pmapper = vtkSmartPointer<vtkPolyDataMapper>::New();
            #if VTK_MAJOR_VERSION <= 5
                pmapper->SetInput(plane);
            #else
                pmapper->SetInputData(plane);
            #endif
            planeActor->SetMapper(pmapper);
            planeActor->GetProperty()->SetOpacity(0.5);
            planeActor->GetProperty()->SetColor(0,1,0);
            assembly->AddPart(planeActor);
            assembly->Modified();
            this->showPlane = true;

        }

        void removePlane(){

            if(showPlane){

                slidingPlane = vtkSmartPointer<vtkPlane>::NewInstance(slidingPlane);
                originalCenter[0] = 0;
                originalCenter[1] = 0;
                originalCenter[2] = 0;
                originalNormal[0] = 0;
                originalNormal[1] = 0;
                originalNormal[2] = 0;
                assembly->RemovePart(planeActor);
                planeActor = vtkSmartPointer<vtkActor>::NewInstance(planeActor);
                this->showPlane = false;

            }

        }

        virtual void OnLeftButtonDown(){

            int mousePosition[2];
            this->Interactor->GetEventPosition(mousePosition);
            if(mousePosition[0] > renderingWidget->x() && (mousePosition[0] < renderingWidget->x() + renderingWidget->width()) &&
               mousePosition[1] > renderingWidget->y() && (mousePosition[1] < renderingWidget->y() + renderingWidget->height()) ){
                if(this->Interactor->GetControlKey()){
                    this->FindPokedRenderer(mousePosition[0], mousePosition[1]);
                    VTK_CREATE(vtkCoordinate,coordinates);
                    coordinates->SetCoordinateSystemToDisplay();
                    coordinates->SetValue(mousePosition[0], mousePosition[1],0);
                    double *p = coordinates->GetComputedWorldValue(this->GetCurrentRenderer());
                    double projectedPoint[3];
                    slidingPlane->ProjectPoint(p, projectedPoint);
                    Point* point1 = new Point(p[0],p[1],p[2]);
                    Point* point2 = new Point(projectedPoint[0],projectedPoint[1],projectedPoint[2]);
                    /*double normal[3], planeNormal[3], po[3];
                    ren->GetActiveCamera()->GetViewPlaneNormal(normal);
                    Eigen::Vector3d lineVector = {normal[0], normal[1], normal[2]};
                    Eigen::Vector3d lineOrigin = {p[0], p[1], p[2]};
                    slidingPlane->GetCenter(po);
                    slidingPlane->GetNormal(planeNormal);
                    Eigen::Vector3d planeVector = {planeNormal[0], planeNormal[1], planeNormal[2]};
                    Eigen::Vector3d planeOrigin = {po[0], po[1], po[2]};
                    Point* point = Utilities::linePlaneIntersection(lineVector, lineOrigin, planeVector, planeOrigin);
                    */
                    VTK_CREATE(vtkCellArray, vertices);
                    pids.push_back(cagePoints->InsertNextPoint(point1->x, point1->y, point1->z));
                    pids.push_back(cagePoints->InsertNextPoint(point2->x, point2->y, point2->z));
                    vertices->InsertNextCell(pids.size(), &pids[0]);
                    ren->RemoveActor(assembly);
                    assembly->RemovePart(pointsActor);
                    VTK_CREATE(vtkPolyData, pointsData);
                    VTK_CREATE(vtkPolyDataMapper, pointsMapper);
                    pointsData->SetPoints(cagePoints);
                    pointsData->SetVerts(vertices);

                    #if VTK_MAJOR_VERSION <= 5
                        pointsMapper->SetInput(pointsData);
                    #else
                        pointsMapper->SetInputData(pointsData);
                    #endif

                    pointsActor->SetMapper(pointsMapper);
                    pointsActor->GetProperty()->SetRepresentationToPoints();
                    pointsActor->GetProperty()->SetPointSize(10.0f);
                    pointsActor->GetProperty()->SetColor(1.0,0,0);
                    assembly->AddPart(pointsActor);
                    assembly->Modified();
                    ren->AddActor(assembly);
                    this->GetCurrentRenderer()->Render();
                    this->GetCurrentRenderer()->GetRenderWindow()->Render();

                } else
                    moving = true;
            }
            vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
        }

        virtual void OnLeftButtonUp(){
            moving = false;
            vtkInteractorStyleTrackballCamera::OnLeftButtonUp();
        }

        virtual void OnMouseMove(){

            if(moving){
                assembly->RemovePart(planeActor);
                slidingPlane->SetOrigin(originalCenter);
                double normal[3];
                ren->GetActiveCamera()->GetViewPlaneNormal(normal);
                slidingPlane->SetNormal(normal);
                ps->SetCenter(slidingPlane->GetOrigin());
                ps->SetNormal(slidingPlane->GetNormal());
                ps->Update();
                vtkSmartPointer<vtkPolyData> plane = ps->GetOutput();
                vtkSmartPointer<vtkPolyDataMapper> pmapper = vtkSmartPointer<vtkPolyDataMapper>::New();
                #if VTK_MAJOR_VERSION <= 5
                    pmapper->SetInput(plane);
                #else
                    pmapper->SetInputData(plane);
                #endif
                planeActor->SetMapper(pmapper);
                planeActor->GetProperty()->SetOpacity(0.5);
                planeActor->GetProperty()->SetColor(0,1,0);
                assembly->AddPart(planeActor);
                ren->Render();
                ren->GetRenderWindow()->Render();
                planeTranslationSlider->setValue(SLIDER_ORIGINAL_VALUE);
            }
            vtkInteractorStyleTrackballCamera::OnMouseMove();
        }

};

#endif // GENERATIONSTYLE_H
