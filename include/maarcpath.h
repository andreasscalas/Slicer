#ifndef MAARCPATH_H
#define MAARCPATH_H

#include "node.h"
#include "imatistl.h"

namespace AndreasStructures {

    class MAArcPath
    {
        public:
            MAArcPath();

            void addPoint(const IMATI_STL::Point &point);
            std::vector<IMATI_STL::Point> getPath() const;
            void setPath(const std::vector<IMATI_STL::Point> &value);

            AndreasStructures::Node *getN1() const;
            void setN1(AndreasStructures::Node *value);

            AndreasStructures::Node *getN2() const;
            void setN2(AndreasStructures::Node *value);

    protected:
            AndreasStructures::Node* n1;
            AndreasStructures::Node* n2;
            std::vector<IMATI_STL::Point> path;
    };

}

#endif // MAARCPATH_H
