#ifndef NODE_H
#define NODE_H

#include <vector>
namespace AndreasStructures{
    class Node{

        public:
            Node();

            std::vector<Node *> getConnectedNodes() const;
            void setConnectedNodes(const std::vector<Node *> &value);
            void addConnectedNode(Node* value);

            void *getData() const;
            void setData(void *value);

            int getKey() const;
            void setKey(int value);

        protected:
            std::vector<Node*> connectedNodes;
            void* data;
            int key;
    };
}
#endif // NODE_H
