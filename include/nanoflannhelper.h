#ifndef NANOFLANNHELPER_H
#define NANOFLANNHELPER_H

#include <vector>
#include "drawablemesh.h"
#include "nanoflann-master/examples/KDTreeVectorOfVectorsAdaptor.h"
#include "nanoflann-master/include/nanoflann.hpp"

using namespace IMATI_STL;
using namespace std;

class NanoflannHelper{

public:
    NanoflannHelper(DrawableMesh* mesh);
    vector<Vertex*> getNeighboursInSphere(Point queryPt, double radius);

protected:
    typedef vector<vector<double> > my_vector_of_vectors_t;
    typedef KDTreeVectorOfVectorsAdaptor< my_vector_of_vectors_t, double > my_kd_tree_t;
    typedef double num_t;

    my_vector_of_vectors_t points;
    my_kd_tree_t* mat_index;
    DrawableMesh* mesh;

};

#endif // NANOFLANNHELPER_H
