#ifndef MAINTHREAD_H
#define MAINTHREAD_H

/**
 * @brief The MainThread class abstract class to define an inetrface for different types of threads
 */
class MainThread{

    public:
        virtual void startThread() = 0;
        virtual void waitThread() = 0;

    protected:
        virtual void executeTask() = 0;
};

#endif // MAINTHREAD_H
