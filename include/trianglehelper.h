#ifndef TRIANGLEHELPER_H
#define TRIANGLEHELPER_H

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <drawablemesh.h>

class TriangleHelper
{
public:
    TriangleHelper(std::vector<IMATI_STL::Point> boundary, std::vector<std::vector<IMATI_STL::Point> > holes);

    std::vector<IMATI_STL::Triangle *> getTriangles() const;

    DrawableMesh *getSliceMesh() const;

private:
    std::vector<IMATI_STL::Point> boundary;
    std::vector<std::vector<IMATI_STL::Point> > holes;
    std::map<long, IMATI_STL::Point*> idVertex;
    const std::string filename = "tmp";
    DrawableMesh* sliceMesh;
    void writePoly();
    void loadELE();
};

#endif // TRIANGLEHELPER_H
