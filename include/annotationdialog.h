#ifndef ANNOTATIONDIALOG_H
#define ANNOTATIONDIALOG_H

#include <QDialog>
#include <QColorDialog>
#include <QColor>
#include <QFileDialog>
#include <QDebug>
#include <string>

namespace Ui {
    class AnnotationDialog;
}

class AnnotationDialog : public QDialog{
    Q_OBJECT

    public:
        AnnotationDialog(QWidget* parent);
        ~AnnotationDialog();

    private slots:
        void slotColor();
        void slotOntologyDialog();
        void slotTagEdit(QString);
        void slotSave();

    signals:
        void finalizationCalled(std::string, unsigned char*);

    private:
        Ui::AnnotationDialog *ui;
        unsigned char color[3];
        std::string tag;
};

#endif // ANNOTATIONDIALOG_H
