#ifndef TRIANGLEHELPER_H
#define TRIANGLEHELPER_H

#include <imatistl.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <drawablemesh.h>

class Poly2TriHelper{
    public:
        Poly2TriHelper(std::vector<std::vector<IMATI_STL::Vertex*> > boundaries);

        std::vector<IMATI_STL::Triangle *> getTriangles() const;

        DrawableMesh *getSliceMesh() const;

    private:
        std::vector<std::vector<IMATI_STL::Vertex*> > boundaries;
        std::map<long, IMATI_STL::Vertex*> idVertex;
        const std::string filename = "tmp";
        DrawableMesh* sliceMesh;
        void writeBDM();
        void loadELE();

};

#endif // TRIANGLEHELPER_H
