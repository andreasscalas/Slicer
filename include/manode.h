#ifndef MANODE_H
#define MANODE_H

#include "node.h"
#include "maarcpath.h"
#include "imatistl.h"
#include <algorithm>

namespace AndreasStructures {

    class MANode : public Node{

        public:
            MANode();


            IMATI_STL::Point* getPoint() const;
            void setPoint(const IMATI_STL::Point value);

            void addPath(AndreasStructures::MAArcPath*);
            std::vector<AndreasStructures::MAArcPath *> getPaths() const;
            void setPaths(const std::vector<AndreasStructures::MAArcPath *> &value);

            bool getMarkedFlag() const;
            void setMarkedFlag(bool value);

            bool getVisitedFlag() const;
            void setVisitedFlag(bool value);

            std::vector<std::pair<MANode*, MANode*> > getGraphArcs();
            std::vector<IMATI_STL::Point> getCommonPath(MANode* other);

    protected:
            IMATI_STL::Point* point;
            std::vector<AndreasStructures::MAArcPath*> paths;
            bool visitedFlag;
            bool markedFlag;

    };


}
#endif // MANODE_H
