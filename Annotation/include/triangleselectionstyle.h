#ifndef TRIANGLESELECTIONSTYLE_H
#define TRIANGLESELECTIONSTYLE_H

#include <vtkSmartPointer.h>
#include <vtkCellPicker.h>
#include <vtkActor.h>
#include <vtkInteractorStyleRubberBandPick.h>
#include <vtkCamera.h>
#include <vtkRenderedAreaPicker.h>
#include <vtkExtractGeometry.h>
#include <vtkSelectVisiblePoints.h>
#include <vtkCellData.h>
#include <vtkFrustumSource.h>
#include <vtkParametricFunctionSource.h>
#include <vtkParametricSpline.h>
#include <vtkSphereSource.h>
#include <vector>
#include <string>
#include "drawablemesh.h"
#include "include/utilities.h"
#define VTKISRBP_ORIENT 0
#define VTKISRBP_SELECT 1

/**
 * @brief The TriangleSelectionStyle class controls the interaction with the Triangles of a mesh
 */
class TriangleSelectionStyle : public vtkInteractorStyleRubberBandPick{

    public:
        static const unsigned int RECTANGLE_AREA = 0;
        static const unsigned int LASSO_AREA = 1;
        static const unsigned int PAINTED_LINE = 2;
        static const double TOLERANCE = 0.05;
        static const double RADIUS_RATIO = 100;

        static TriangleSelectionStyle* New();
        TriangleSelectionStyle();
        vtkTypeMacro(TriangleSelectionStyle, vtkInteractorStyleRubberBandPick)

        void OnRightButtonDown();
        void OnMouseMove();
        void OnLeftButtonDown();
        void OnLeftButtonUp();
        void modifySelectedTriangles();
        void modifySelectedTrianglesId();
        void SetTriangles(vtkSmartPointer<vtkPolyData> triangles);
        void resetSelection();

        Annotation *getAnnotation() const;
        DrawableMesh *getMesh() const;
        void setMesh(DrawableMesh *value);
        std::vector<Vertex *> getPolygonContour() const;
        void setPolygonContour(const std::vector<Vertex *> &value);
        Vertex *getInnerVertex() const;
        void setInnerVertex(Vertex *value);
        unsigned int getSelectionType() const;
        void setSelectionType(unsigned int value);
        bool getShowSelectedTriangles() const;
        void setShowSelectedTriangles(bool value);
        bool getVisibleTrianglesOnly() const;
        void setVisibleTrianglesOnly(bool value);
        bool getSelectionMode() const;
        void setSelectionMode(bool value);
        vtkSmartPointer<vtkRenderer> getRen() const;
        void setRen(const vtkSmartPointer<vtkRenderer> &value);
        std::vector<Triangle*> *getSelectedTriangles() const;
        void setSelectedTriangles(std::vector<Triangle*> *value);
        std::vector<long int> *getSelectedTrianglesId() const;
        void setSelectedTrianglesId(std::vector<long int> *value);
        vtkSmartPointer<vtkAssembly> getAssembly() const;
        void setAssembly(const vtkSmartPointer<vtkAssembly> &value);
        void finalizeAnnotation(string tag, unsigned char color[]);

private:
        vtkSmartPointer<vtkAssembly> assembly;          //Assembly of actors
        vtkSmartPointer<vtkAssembly> sphereAssembly;    //Assembly of spheres
        vtkSmartPointer<vtkPolyData> Triangles;
        vtkSmartPointer<vtkActor> SplineActor;
        std::vector<long>* SelectedPoints;
        std::vector<Triangle*>* SelectedTriangles;
        std::vector<long>* SelectedTrianglesId;
        vtkSmartPointer<vtkCellPicker> cellPicker;      //The cell picker
        vtkSmartPointer<vtkPoints> splinePoints;
        vtkSmartPointer<vtkParametricSpline> spline;
        vtkSmartPointer<vtkRenderer> ren;
        double sphereRadius;
        bool selectionMode;
        bool visibleTrianglesOnly;
        bool showSelectedTriangles;
        bool alreadyStarted;
        bool lasso_started;
        short selectionType;
        Annotation* annotation;
        DrawableMesh* mesh;
        Vertex* firstVertex;
        Vertex* lastVertex;
        Vertex* innerVertex;
        vector<Vertex*> polygonContour;

};

#endif // TRIANGLESELECTIONSTYLE_H
