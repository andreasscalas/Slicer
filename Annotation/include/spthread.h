#ifndef SPTHREAD_H
#define SPTHREAD_H

#include <vector>
#include <thread>
#include "mainthread.h"
#include "drawablemesh.h"
#include "utilities.h"
#include <qdebug.h>

using namespace std;

class SPThread{

    private:
        thread* tid;            //Thread ID
        Vertex* v1, *v2;         //Vertices between which the path will be computed
        vector<Vertex*>* path;  //The computed path

        /**
         * @brief executeTask method that allows the execution of the main task of the thread
         */
        void executeTask();

        /**
         * @brief executeTaskHelper method that allows the running of executeTask method
         * @param context the contest on which there is executeTask method
         * @return if necessairy, some return value of the task
         */
        static void* executeTaskHelper(void* context);


    public:

        /**
         * @brief GCThread main constructor of the class. To succesfully start, a thread need a set of information
         * @param v1 First vertex of the path
         * @param v2 Last vertex of the path
         * @param path The computed path
         */
        SPThread(Vertex* v1, Vertex* v2, vector<Vertex*>* path);

        /**
         * @brief startThread method that allows the starting of the thread execution
         */
        void startThread();

        /**
         * @brief waitThread method that allows the request for waiting the end of the thread execution
         */
        void waitThread();

};

#endif // SPTHREAD_H
