/********************************************
 * Copyright (c) 2017 CNR-IMATI Ge
 * Author: Andrea Repetto
 * All rights reserved
 ********************************************/
#ifndef FACET_H
#define FACET_H

#include <vector>
#include <string>
#include <stdexcept>
#include <iostream>
#include <map>

std::string trim(const std::string &str);

class fct_parse_error : public std::runtime_error
{
public:
    fct_parse_error(std::string __arg) :
        std::runtime_error(__arg)
    {
    }
};

struct Facet
{
    enum class FacetType
    {
        INTERNAL,
        EXTERNAL,
        FRACTURE,
        UNKNOWN
    };


    // The label of the facet, typically a string containing an integer number
    std::string label;

    // Indices of the boundary vertices
    std::vector<std::vector<int> > boundaryVertices;

    // Indices of the inner vertices
    std::vector<int> innerVertices;

    // The facet type
    FacetType type;

};

class FacetReader{

    public:
        bool readFct(const std::string &filename);

        std::vector<Facet> getFacets();


    private:
        void readHeader(std::fstream &fs);

        void readFacet(std::fstream &fs);

        std::string readField(std::istream &fs, const std::string &regex, int matchIndex = 1);

        void readExact(std::istream &fs, const std::string &match_str);

        template <typename T>
        std::vector<T> readVector(std::istream &fs)
        {
            try
            {
                std::string line;

                std::getline(fs, line);

                std::istringstream ss(line);

                std::vector<T> out;

                T value;

                if(line.compare("Vertices:")==0)
                    return out;

                while(ss >> value)
                {
                    out.push_back( value - T(1) );
                }
                return out;
            }
            catch(std::exception &e)
            {
                throw fct_parse_error("ERROR Could not read vector input");
            }
        }

        Facet::FacetType strToFacetType(std::string &value);

        std::string m_filename;

        std::vector<Facet> m_facets;

        int m_numFacets = 0;

        std::map<std::string, Facet::FacetType> m_facetTypeMap =
        {
            { "external", Facet::FacetType::EXTERNAL },
            { "internal", Facet::FacetType::INTERNAL},
            { "fracture", Facet::FacetType::FRACTURE },
        };


};


#endif // FACET_H
