#ifndef UTILITIES_ANNOTATION_H
#define UTILITIES_ANNOTATION_H

#include <vector>
#include <queue>
#include <map>
#include <math.h>
#include <iostream>
#include <cstdio>
#include <ctime>
#include "drawablemesh.h"

using namespace IMATI_STL;
using namespace std;

class DrawableMesh;
namespace Utilities {

    const double EPSILON_BC = 0.005;
    const double EPSILON_D = 0.01;
    const short int SIMPLE_SEGMENT_DISTANCE = 1;
    const short int WEIGHTED_SEGMENT_DISTANCE = 2;
    const short int EUCLIDEAN_DISTANCE = 2;

    template<class T>
    bool isPairInList(std::pair<T,T> p, std::vector<std::pair<T,T> > v){
        for(int i = 0; i < v.size(); i++)
            if((p.first == v[i].second && p.second == v[i].first) ||
               (p.first == v[i].second && p.second == v[i].first))
                return true;
        return false;
    }

    Vertex* vertexInList(Vertex*, List*);
    Triangle* triangleInList(Triangle* t, List* l);
    void checkOutlineOrder(vector<int> innerVerticesIndices, vector<Vertex *> &outline, DrawableMesh* model);
    bool isPointInsideTriangle(Point checkPoint, Point trianglePoint1, Point trianglePoint2, Point trianglePoint3);
    vector<Vertex*> geodesicDijkstra(Vertex* v1, Vertex* v2);
    vector<Vertex*> linearDijkstra(Vertex* v1, Vertex* v2);
    vector<Triangle*> regionGrowing(vector<vector<Vertex*> > contours);
    Vertex* findCorrespondingVertex(Vertex* v, vector<Triangle*> neighbors);
    Triangle* findCorrespondingTriangle(Vertex* v, vector<Triangle*> neighbors);
    vector<Vertex*> getOutline(vector<Triangle*> set);
    vector<vector<Vertex*> > getOutlines(vector<Triangle*> set);

    void findFaces(vector<Triangle*> &faces, vector<Vertex*> vertices);
}

#endif
