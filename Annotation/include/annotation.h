#ifndef ANNOTATION_H
#define ANNOTATION_H

#include <vector>
#include <queue>
#include <fstream>
#include <string>
#include <sstream>
#include <string>
#include <QDebug>
#include "drawablemesh.h"
#include "utilities.h"
#include "spthread.h"
#include "nanoflann-master/include/nanoflann.hpp"
#include "nanoflannhelper.h"

using namespace std;
using namespace IMATI_STL;

class DrawableMesh;
class Annotation {

    public:
        Annotation();
        /**
         * @brief This method takes the annotations of an object defined on
         * a model with a certain resolution and transfers it to a model with another resolution
         * @param otherMesh The model with lower resolution
         * @return The annotation defined on the other model.
         */
        Annotation* transfer(DrawableMesh* otherMesh);

        /**
         * @brief This method takes the annotations of an object defined on
         * a model with a certain resolution and transfers it to a model with another resolution using threads
         * @param otherMesh The model with lower resolution
         * @return The annotation defined on the other model.
         */
        Annotation* parallelTransfer(DrawableMesh* otherMesh);

        /**
         * @brief This method allow to refresh the area value of the annotation
         */
        void updateArea();

        /**
         * @brief This method allow to refresh the perimeter value of the annotation
         */
        void updatePerimeter();

        //Getter and setter methods
        vector<vector<Vertex *> > getOutlines() const;
        void setOutlines(const vector<vector<Vertex *> > value);
        void addOutline(const vector<Vertex*> value);
        string getTag() const;
        void setTag(string value);
        unsigned char* getColor() const;
        void setColor(unsigned char value[3]);
        vector<Triangle*> getTriangles();
        double getArea() const;
        double getPerimeter() const;

private:
        vector<vector<Vertex*> > outlines;      //The outline of the annotated region
        Triangle* innerTriangle;                //A point inside the annotated region (used as seed for region growing)
        unsigned char color[3];                 //The color associated to the annotation
        string tag;                             //The tag of the annotation
        double area;                            //The area covered by the annotation
        double perimeter;                       //The perimeter of the annotation
        double sphere_ray;                      //The radius of the sphere for the neighborhood search
        const short BBOX_SPHERE_RATIO = 1000;   //Divisive coefficient between the BBox longest diagonal and neighborhood sphere radius
        const short NUM_OF_THREADS = 8;         //Number of threads used for the transfer procedure
        const bool ORDER = true;                //Order of the outline: if TRUE then it is counterclockwise, otherwise is clockwise


};

#endif // ANNOTATION_H
