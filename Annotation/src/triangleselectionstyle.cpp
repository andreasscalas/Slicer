#include "triangleselectionstyle.h"

TriangleSelectionStyle::TriangleSelectionStyle(){
    selectionMode = true;
    selectionType = RECTANGLE_AREA;
    visibleTrianglesOnly = true;
    lasso_started = false;
    alreadyStarted = false;
    showSelectedTriangles = true;
    firstVertex = nullptr;
    lastVertex = nullptr;
    this->annotation = new Annotation;
    splinePoints = vtkSmartPointer<vtkPoints>::New();
    SplineActor  = vtkSmartPointer<vtkActor>::New();
    SplineActor->GetProperty()->SetColor(1.0,0,0);
    SplineActor->GetProperty()->SetLineWidth(3.0);
    sphereAssembly = vtkSmartPointer<vtkAssembly>::New();          //Assembly of actors
    this->cellPicker = vtkSmartPointer<vtkCellPicker>::New();
}

void TriangleSelectionStyle::OnRightButtonDown(){

    //If the user is trying to pick a point...
    //The click position of the mouse is takenannotatedTriangles
    double x, y;
    x = this->Interactor->GetEventPosition()[0];
    y = this->Interactor->GetEventPosition()[1];
    this->FindPokedRenderer(x, y);
    //Some tolerance is set for the picking
    this->cellPicker->SetTolerance(0);
    this->cellPicker->Pick(x, y, 0, ren);
    //If some point has been picked...
    vtkIdType pickedTriangleID = this->cellPicker->GetCellId();
    if(pickedTriangleID > 0 && pickedTriangleID < this->mesh->T.numels()){

        if(lasso_started){
            Triangle* t = mesh->getTriangleByID(pickedTriangleID);
            //this->annotation->setInnerTriangle(t);
            this->annotation->addOutline(polygonContour);

            vector<Triangle*> innerTriangles = Utilities::regionGrowing(polygonContour, t);
            for(vector<Triangle*>::iterator tit = innerTriangles.begin(); tit != innerTriangles.end(); tit++){
                SelectedTriangles->push_back((Triangle*) *tit);
                SelectedTrianglesId->push_back(mesh->getTriangleID((Triangle*) *tit));
            }
            splinePoints = vtkSmartPointer<vtkPoints>::New();
            assembly->RemovePart(sphereAssembly);
            sphereAssembly = vtkSmartPointer<vtkAssembly>::New();
            polygonContour.clear();
            lastVertex = nullptr;
            firstVertex = nullptr;
            lasso_started = false;
            this->assembly->RemovePart(SplineActor);
        }else{
            std::vector<long int>::iterator it = std::find(SelectedTrianglesId->begin(),SelectedTrianglesId->end(), pickedTriangleID);
            if(it == SelectedTrianglesId->end() && selectionMode){
                SelectedTriangles->push_back(mesh->getTriangleByID(pickedTriangleID));
                SelectedTrianglesId->push_back(pickedTriangleID);
            }else if(it != SelectedTrianglesId->end()){
                std::vector<Triangle*>::iterator nit = std::find(SelectedTriangles->begin(),SelectedTriangles->end(), mesh->getTriangleByID(pickedTriangleID));
                SelectedTriangles->erase(nit);
                SelectedTrianglesId->erase(it);
            }
        }

        modifySelectedTriangles();

    }
}


void TriangleSelectionStyle::OnMouseMove(){

    switch (selectionType) {

        case LASSO_AREA:

            vtkInteractorStyleRubberBandPick::OnMouseMove();
        default:
            vtkInteractorStyleRubberBandPick::OnMouseMove();
    }

}

void TriangleSelectionStyle::OnLeftButtonDown(){

    if(this->Interactor->GetControlKey())
        switch(selectionType){

            case RECTANGLE_AREA:
                this->CurrentMode = VTKISRBP_SELECT;
                break;

            case LASSO_AREA:
                if(!lasso_started){
                    lasso_started = true;
                }else{
                    this->cellPicker->AddPickList(mesh->getCanvas());
                    this->cellPicker->PickFromListOn();
                    //The click position of the mouse is taken
                    int x, y;
                    x = this->Interactor->GetEventPosition()[0];
                    y = this->Interactor->GetEventPosition()[1];
                    this->FindPokedRenderer(x, y);
                    //Some tolerance is set for the picking
                    this->cellPicker->Pick(x, y, 0, ren);
                    vtkIdType cellID = this->cellPicker->GetCellId();

                    //If some point has been picked...
                    if(cellID > 0 && cellID < this->mesh->T.numels()){
                        Triangle* t = mesh->getTriangleByID(cellID);
                        double wc[3];
                        double bestDistance = DBL_MAX;

                        this->cellPicker->GetPickPosition(wc);
                        Vertex* p = new Vertex(wc[0], wc[1], wc[2]);
                        Vertex* tv1 = t->v1(), *tv2 = t->v2(), *tv3 = t->v3();
                        double* normal = this->GetCurrentRenderer()->GetActiveCamera()->GetViewPlaneNormal();
                        Point n(normal[0], normal[1], normal[2]);
                        Point p_ = Point::linePlaneIntersection(*p, (*p) + n, *tv1, *tv2, *tv3);
                        Vertex* v_ = t->v1();
                        Vertex* v;

                        for(int i = 0; i < 3; i++){
                            double actualDistance = ((p_)-(*p)).length();
                            if(actualDistance < bestDistance){
                                bestDistance = actualDistance;
                                v = v_;
                            }
                            v_ = t->nextVertex(v_);
                        }

                        vtkIdType pointID = mesh->getPointID(v);
                        vtkSmartPointer<vtkParametricSpline> spline = vtkSmartPointer<vtkParametricSpline>::New();
                        Vertex* actualVertex = mesh->getPointByID(pointID);
                        vtkSmartPointer<vtkSphereSource> sphereSource = vtkSmartPointer<vtkSphereSource>::New();
                        sphereSource->SetCenter(actualVertex->x, actualVertex->y, actualVertex->z);
                        sphereSource->SetRadius(sphereRadius);
                        vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
                        mapper->SetInputConnection(sphereSource->GetOutputPort());
                        vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
                        actor->GetProperty()->SetColor(0,0,1);
                        actor->SetMapper(mapper);
                        sphereAssembly->AddPart(actor);
                        assembly->AddPart(sphereAssembly);
                        if(firstVertex == nullptr){
                            firstVertex = actualVertex;
                            polygonContour.push_back(firstVertex);
                        }if(lastVertex != nullptr && lastVertex != actualVertex){
                            vector<Vertex*> newContourSegment = Utilities::linearDijkstra(lastVertex, actualVertex);
                            polygonContour.insert(polygonContour.end(), newContourSegment.begin(), newContourSegment.end());
                            for(int i = 0; i < newContourSegment.size(); i++)
                                splinePoints->InsertNextPoint(Triangles->GetPoint(mesh->getPointID(newContourSegment[i])));
                            spline->SetPoints(splinePoints);
                            vtkSmartPointer<vtkParametricFunctionSource> functionSource = vtkSmartPointer<vtkParametricFunctionSource>::New();
                            functionSource->SetParametricFunction(spline);
                            functionSource->Update();
                            ren->RemoveActor(assembly);
                            this->assembly->RemovePart(SplineActor);
                            // Setup actor and mapper
                            vtkSmartPointer<vtkPolyDataMapper> splineMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
                            splineMapper->SetInputConnection(functionSource->GetOutputPort());
                            SplineActor->SetMapper(splineMapper);
                            this->assembly->AddPart(SplineActor);
                            ren->AddActor(assembly);
                            ren->Render();
                            ren->GetRenderWindow()->Render();
                        }
                        lastVertex = actualVertex;
                    }
                }

                break;

            case PAINTED_LINE:
                break;

            default: break;
        }

    vtkInteractorStyleRubberBandPick::OnLeftButtonDown();
}

void TriangleSelectionStyle::OnLeftButtonUp(){

    vtkInteractorStyleRubberBandPick::OnLeftButtonUp();
    switch(selectionType){
        case RECTANGLE_AREA:
            if(this->CurrentMode==VTKISRBP_SELECT){
                this->CurrentMode = VTKISRBP_ORIENT;

                // Forward events

                vtkPlanes* frustum = static_cast<vtkRenderedAreaPicker*>(this->GetInteractor()->GetPicker())->GetFrustum();

                vtkSmartPointer<vtkExtractGeometry> extractGeometry = vtkSmartPointer<vtkExtractGeometry>::New();
                extractGeometry->SetImplicitFunction((vtkImplicitFunction*) frustum);

                #if VTK_MAJOR_VERSION <= 5
                    extractGeometry->SetInput(this->Triangles);
                #else
                    extractGeometry->SetInputData(this->Triangles);
                #endif
                    extractGeometry->Update();

                vtkSmartPointer<vtkVertexGlyphFilter> glyphFilter = vtkSmartPointer<vtkVertexGlyphFilter>::New();
                glyphFilter->SetInputConnection(extractGeometry->GetOutputPort());
                glyphFilter->Update();

                vtkSmartPointer<vtkPolyData> selected;
                if(visibleTrianglesOnly){
                    vtkSmartPointer<vtkSelectVisiblePoints> selectVisiblePoints = vtkSmartPointer<vtkSelectVisiblePoints>::New();
                    selectVisiblePoints->SetInputConnection(glyphFilter->GetOutputPort());
                    selectVisiblePoints->SetRenderer(ren);
                    selectVisiblePoints->Update();
                    selected = selectVisiblePoints->GetOutput();
                }else
                    selected = glyphFilter->GetOutput();


                int TRIANGLE_SELECTED = 9898;
                for(vector<Triangle*>::iterator it = SelectedTriangles->begin(); it!= SelectedTriangles->end(); it++)
                    (*it)->info = &TRIANGLE_SELECTED;

                vtkSmartPointer<vtkIdTypeArray> ids = vtkIdTypeArray::SafeDownCast(selected->GetPointData()->GetArray("OriginalIds"));
                for(vtkIdType i = 0; ids != nullptr && i < ids->GetNumberOfTuples(); i++){
                    vtkSmartPointer<vtkIdList> tids = vtkSmartPointer<vtkIdList>::New();
                    Triangles->GetPointCells(ids->GetValue(i), tids);
                    for(vtkIdType j = 0; j < tids->GetNumberOfIds(); j++){
                        Triangle* t = mesh->getTriangleByID(tids->GetId(j));
                        if((t->info == nullptr || *static_cast<int*>(t->info) != TRIANGLE_SELECTED) && selectionMode){
                            SelectedTrianglesId->push_back(tids->GetId(j));
                            SelectedTriangles->push_back(t);
                            t->info = &TRIANGLE_SELECTED;
                        }

                        if((t->info != nullptr && *static_cast<int*>(t->info) == TRIANGLE_SELECTED) && !selectionMode){
                            std::vector<long int>::iterator sit = std::find(SelectedTrianglesId->begin(), SelectedTrianglesId->end(), tids->GetId(j));
                            std::vector<Triangle*>::iterator nit = std::find(SelectedTriangles->begin(), SelectedTriangles->end(), t);
                            SelectedTrianglesId->erase(sit);
                            SelectedTriangles->erase(nit);
                            t->info = nullptr;
                        }
                    }
                }

                for(vector<Triangle*>::iterator it = SelectedTriangles->begin(); it!= SelectedTriangles->end(); it++)
                    (*it)->info = nullptr;
                /*
                vector<long> toAdd;
                vector<long> toRemove;
                for(vector<long>::iterator it = newlySelected.begin(); it != newlySelected.end(); it++){
                    std::vector<long>::iterator sit = std::find(SelectedTrianglesId->begin(), SelectedTrianglesId->end(),  *it);
                    if(sit == SelectedTrianglesId->end() && selectionMode)
                        toAdd.push_back(*it);
                    else if(sit != SelectedTrianglesId->end() && !selectionMode )
                        toRemove.push_back(*it);
                }

                for(vector<long int>::iterator it = toRemove.begin(); it != toRemove.end(); it++){
                    std::vector<long int>::iterator sit = std::find(SelectedTrianglesId->begin(), SelectedTrianglesId->end(), *it);
                    std::vector<Triangle*>::iterator nit = std::find(SelectedTriangles->begin(), SelectedTriangles->end(), mesh->getTriangleByID(*it));
                    if(sit != SelectedTrianglesId->end() && nit != SelectedTriangles->end()){
                        SelectedTrianglesId->erase(sit);
                        SelectedTriangles->erase(nit);
                    }
                }

                for(vector<long int>::iterator it = toAdd.begin(); it != toAdd.end(); it++){
                    SelectedTrianglesId->push_back(*it);
                    SelectedTriangles->push_back(mesh->getTriangleByID(*it));
                }
                */

                modifySelectedTriangles();
            }
            break;

    }

}

void TriangleSelectionStyle::modifySelectedTriangles(){

    if(showSelectedTriangles)
        mesh->setSelectedTriangles(*SelectedTrianglesId);
    else{
        std::vector<long int> empty_vector;
        mesh->setSelectedTriangles(empty_vector);
    }
    ren->RemoveActor(assembly);
    this->mesh->draw(assembly);
    ren->AddActor(assembly);
    ren->Render();
    ren->GetRenderWindow()->Render();;

}

void TriangleSelectionStyle::SetTriangles(vtkSmartPointer<vtkPolyData> triangles) {this->Triangles = triangles;}

void TriangleSelectionStyle::resetSelection(){
    this->SelectedTriangles->clear();
    this->SelectedTrianglesId->clear();
    modifySelectedTriangles();
}

Annotation *TriangleSelectionStyle::getAnnotation() const{
    return annotation;
}

DrawableMesh *TriangleSelectionStyle::getMesh() const{
    return mesh;
}

void TriangleSelectionStyle::setMesh(DrawableMesh *value){
    mesh = value;
    this->sphereRadius = this->mesh->getBoundingBallRadius() / RADIUS_RATIO;
}

std::vector<Vertex *> TriangleSelectionStyle::getPolygonContour() const{
    return polygonContour;
}

void TriangleSelectionStyle::setPolygonContour(const std::vector<Vertex *> &value){
    polygonContour = value;
}

void TriangleSelectionStyle::setInnerVertex(Vertex *value){
    innerVertex = value;
}

unsigned int TriangleSelectionStyle::getSelectionType() const{
    return selectionType;
}

void TriangleSelectionStyle::setSelectionType(unsigned int value){
    selectionType = value;
}

bool TriangleSelectionStyle::getShowSelectedTriangles() const{
    return showSelectedTriangles;
}

void TriangleSelectionStyle::setShowSelectedTriangles(bool value){
    showSelectedTriangles = value;
}

bool TriangleSelectionStyle::getVisibleTrianglesOnly() const{
    return visibleTrianglesOnly;
}

void TriangleSelectionStyle::setVisibleTrianglesOnly(bool value){
    visibleTrianglesOnly = value;
}

bool TriangleSelectionStyle::getSelectionMode() const{
    return selectionMode;
}

void TriangleSelectionStyle::setSelectionMode(bool value){
    selectionMode = value;
}

vtkSmartPointer<vtkRenderer> TriangleSelectionStyle::getRen() const{
    return ren;
}

void TriangleSelectionStyle::setRen(const vtkSmartPointer<vtkRenderer> &value){
    ren = value;
}

std::vector<Triangle*> *TriangleSelectionStyle::getSelectedTriangles() const{
    return SelectedTriangles;
}

void TriangleSelectionStyle::setSelectedTriangles(std::vector<Triangle*> *value){
    SelectedTriangles = value;
}

std::vector<long int> *TriangleSelectionStyle::getSelectedTrianglesId() const{
    return SelectedTrianglesId;
}

void TriangleSelectionStyle::setSelectedTrianglesId(std::vector<long int> *value){
    SelectedTrianglesId = value;
}

vtkSmartPointer<vtkAssembly> TriangleSelectionStyle::getAssembly() const{
    return assembly;
}

void TriangleSelectionStyle::setAssembly(const vtkSmartPointer<vtkAssembly> &value){
    assembly = value;
}

Vertex* TriangleSelectionStyle::getInnerVertex() const{
    return innerVertex;
}

void TriangleSelectionStyle::finalizeAnnotation(string tag, unsigned char color[]){
    if(SelectedTriangles->size() > 0){
        this->annotation->setOutlines(Utilities::getOutlines(*SelectedTriangles));
        this->annotation->setColor(color);
        this->annotation->setTag(tag);
        this->mesh->addAnnotation(annotation);
        this->mesh->setAnnotationsModified(true);
        this->mesh->update();
        this->mesh->draw(assembly);
        this->SelectedTriangles->clear();
        this->SelectedTrianglesId->clear();
        this->modifySelectedTriangles();
        this->annotation = new Annotation();
    }
}
