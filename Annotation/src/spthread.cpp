#include "spthread.h"

SPThread::SPThread(Vertex* v1, Vertex* v2, vector<Vertex*>* path){
    this->v1 = v1;
    this->v2 = v2;
    this->path = path;
}

void SPThread::executeTask(){
    *path = Utilities::linearDijkstra(v1,v2);
}

static void* SPThread::executeTaskHelper(void* context){ ((SPThread*) context)->executeTask(); }

void SPThread::startThread(){ tid = new thread(executeTaskHelper, this); }

void SPThread::waitThread(){ try{tid->join();} catch(int e){qDebug()<<e;} }
