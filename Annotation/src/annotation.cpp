#include "annotation.h"

Annotation::Annotation() {
    color[0] = 0;
    color[1] = 0;
    color[2] = 0;
    tag = "";
    area = 0.0;
    perimeter = 0.0;
    sphere_ray = 0.0;
}

Annotation* Annotation::transfer(DrawableMesh* targetMesh){

    Annotation* otherAnnotation = new Annotation(); //The transferred annotation
    std::vector<Vertex*> otherOutline;              //The outline of the transferred annotation
    Vertex* v, *initialVertex;                      //Some support variable

    std::clock_t start;
    int duration;
    start = std::clock();
    NanoflannHelper h(targetMesh);
    sphere_ray = targetMesh->bboxLongestDiagonal() / BBOX_SPHERE_RATIO;

    for(vector<vector<Vertex*> >::iterator oit = outlines.begin(); oit != outlines.end(); oit++){

        vector<Vertex*> outline = (vector<Vertex*>) *oit;
        std::vector<Vertex*>::iterator vit = outline.begin();
        Vertex* v1, *v2;
        do{
            v = (Vertex*) *vit;
            vector<Vertex*> neighbors = h.getNeighboursInSphere(*v, sphere_ray);
            vector<Triangle*> toCheckTriangles;
            Utilities::findFaces(toCheckTriangles, neighbors);
            v1 = Utilities::findCorrespondingVertex(v, toCheckTriangles);
            vit++;
        }while(v1 == NULL && vit != outline.end());
        initialVertex = v1;
        otherOutline.push_back(v1);

        for(; vit != outline.end(); vit++){
            v = (Vertex*) *vit;
            vector<Vertex*> neighbors = h.getNeighboursInSphere(*v, sphere_ray);
            vector<Triangle*> toCheckTriangles;
            Utilities::findFaces(toCheckTriangles, neighbors);
            v2 = Utilities::findCorrespondingVertex(v, toCheckTriangles);
            if(v2 != NULL){
                std::vector<Vertex*> path = Utilities::linearDijkstra(v1,v2);
                otherOutline.insert(otherOutline.end(), path.begin(), path.end());
                v1 = v2;
            }
        }

        v2 = initialVertex;
        vector<Vertex*> path = Utilities::linearDijkstra(v1,v2);
        otherOutline.insert(otherOutline.end(), path.begin(), path.end());

        while((otherOutline[0] == otherOutline[otherOutline.size() - 1]) && (otherOutline[1] == otherOutline[otherOutline.size() - 2])){
            otherOutline.erase(otherOutline.begin());
            otherOutline.erase(otherOutline.begin() + otherOutline.size() - 1);
        }

        v = otherOutline[0];
        otherOutline.erase(otherOutline.begin());
        std::vector<Vertex*> crossedVertices;
        int i = 0;
        for(vit = otherOutline.begin(); vit != otherOutline.end(); vit++){
            v1 = (Vertex*) *vit;
            if(std::find(crossedVertices.begin(), crossedVertices.end(), *vit) == crossedVertices.end())
                crossedVertices.push_back(v1);
            else{
                std::vector<Vertex*>::iterator vit1 = vit;
                for(; vit1 != otherOutline.end(); vit1--){
                    v2 = *vit1;
                    if(v2 == v1)
                        break;
                }
                if(v1 == v2)
                    otherOutline.erase(vit,vit1);

            }
        }

        otherOutline.insert(otherOutline.begin(), v);

        otherAnnotation->addOutline(otherOutline);  //The new annotation outline is computed
        otherAnnotation->updateArea();
        otherAnnotation->updatePerimeter();
    }

    otherAnnotation->setTag(this->tag);
    otherAnnotation->setColor(this->color);
    //The outlines and have been found, the tag and color are the same, so the process ends.

    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

    int hours = 0, mins = 0, secs;

    if(duration > 3600){
        hours = duration / 3600;
        duration = duration % 3600;
    }

    if(duration > 60){
        mins = duration / 60;
        duration = duration % 60;
    }

    secs = duration;

    qDebug()<< hours << ":" << mins << ":" << secs << "\n";

    return otherAnnotation;

}

Annotation* Annotation::parallelTransfer(DrawableMesh* targetMesh){

    Annotation* otherAnnotation = new Annotation(); //The transferred annotation
    std::vector<Vertex*> otherOutline;              //The outline of the transferred annotation
    Vertex* v, *initialVertex;                      //Some support variable

    std::clock_t start;
    int duration;
    start = std::clock();
    NanoflannHelper h(targetMesh);
    sphere_ray = targetMesh->bboxLongestDiagonal() / BBOX_SPHERE_RATIO;

    for(vector<vector<Vertex*> >::iterator oit = outlines.begin(); oit != outlines.end(); oit++){
        vector<Vertex*> outline = (vector<Vertex*>) *oit;
        std::vector<Vertex*>::iterator vit = outline.begin();
        Vertex* v1, *v2;
        do{
            v = (Vertex*) *vit;
            vector<Vertex*> neighbors = h.getNeighboursInSphere(*v, sphere_ray);
            vector<Triangle*> toCheckTriangles;
            Utilities::findFaces(toCheckTriangles, neighbors);
            v1 = Utilities::findCorrespondingVertex(v, toCheckTriangles);
            vit++;
        }while(v1 == NULL && vit != outline.end());
        initialVertex = v1;
        otherOutline.push_back(v1);

        vector<SPThread*> spTasks;
        vector<vector<Vertex*> *> paths;


        for(; vit != outline.end(); vit++){
            v = (Vertex*) *vit;
            vector<Vertex*> neighbors = h.getNeighboursInSphere(*v, sphere_ray);
            vector<Triangle*> toCheckTriangles;
            Utilities::findFaces(toCheckTriangles, neighbors);
            v2 = Utilities::findCorrespondingVertex(v, toCheckTriangles);
            if(v2 != NULL){
                if(spTasks.size() >= NUM_OF_THREADS){
                    for(int i = 0; i < NUM_OF_THREADS; i++){
                        spTasks[i]->waitThread();
                        otherOutline.insert(otherOutline.end(), paths[i]->begin(), paths[i]->end());
                    }
                    paths.clear();
                    spTasks.clear();
                }
                paths.push_back(new vector<Vertex*>());
                spTasks.push_back(new SPThread(v1,v2, paths[paths.size() - 1]));
                spTasks[spTasks.size() - 1]->startThread();
                v1 = v2;
            }
        }
        for(int i = 0; i < spTasks.size(); i++){
            spTasks[i]->waitThread();
            otherOutline.insert(otherOutline.end(), paths[i]->begin(), paths[i]->end());
        }
        v2 = initialVertex;
        vector<Vertex*> path = Utilities::linearDijkstra(v1,v2);
        otherOutline.insert(otherOutline.end(), path.begin(), path.end());

        while((otherOutline[0] == otherOutline[otherOutline.size() - 1]) && (otherOutline[1] == otherOutline[otherOutline.size() - 2])){
            otherOutline.erase(otherOutline.begin());
            otherOutline.erase(otherOutline.begin() + otherOutline.size() - 1);
        }

        v = otherOutline[0];
        otherOutline.erase(otherOutline.begin());
        std::vector<Vertex*> crossedVertices;
        for(vit = otherOutline.begin(); vit != otherOutline.end(); vit++){
            v1 = (Vertex*) *vit;
            if(std::find(crossedVertices.begin(), crossedVertices.end(), *vit) == crossedVertices.end())
                crossedVertices.push_back(v1);
            else{
                std::vector<Vertex*>::iterator vit1 = vit;
                vit1++;
                for(; vit1 != otherOutline.end(); vit1--){
                    v2 = *vit1;
                    if(v2 == v1)
                        break;
                }
                if(v1 == v2)
                    otherOutline.erase(vit,vit1);

            }
        }
        otherOutline.insert(otherOutline.begin(), v);

        otherAnnotation->addOutline(otherOutline);  //The new annotation outline is computed
        //The Outline and inner vertex have been found, the tag and color are the same, so the process ends.
        otherAnnotation->setTag(this->tag);
        otherAnnotation->setColor(this->color);
        otherAnnotation->updateArea();
        otherAnnotation->updatePerimeter();
    }

    duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

    int hours = 0, mins = 0, secs;

    if(duration > 3600){
        hours = duration / 3600;
        duration = duration % 3600;
    }

    if(duration > 60){
        mins = duration / 60;
        duration = duration % 60;
    }

    secs = duration;

    qDebug()<< hours << ":" << mins << ":" << secs << "\n";


    return otherAnnotation;

}

vector<vector<Vertex *> > Annotation::getOutlines() const{
    return outlines;
}

void Annotation::setOutlines(const vector<vector<Vertex *> > value){
    outlines.clear();
    outlines.insert(outlines.end(), value.begin(), value.end());
}

void Annotation::addOutline(const vector<Vertex *> value){
    outlines.push_back(value);
}

unsigned char* Annotation::getColor() const{
    return color;
}

void Annotation::setColor(unsigned char value[3]){
    this->color[0] = value[0];
    this->color[1] = value[1];
    this->color[2] = value[2];
}

vector<Triangle *> Annotation::getTriangles(){
    vector<Triangle*> annotationTriangles = Utilities::regionGrowing(outlines);

    return annotationTriangles;
}

double Annotation::getArea() const{
    return area;
}

double Annotation::getPerimeter() const{
    return perimeter;
}

string Annotation::getTag() const{
    return tag;
}

void Annotation::setTag(string value){
    tag = value;
}

void Annotation::updateArea(){
    vector<Triangle*> triangles = getTriangles();
    area = 0;
    for(vector<Triangle*>::iterator tit = triangles.begin(); tit != triangles.end(); tit++)
        area += (*tit)->area();
}

void Annotation::updatePerimeter(){

    for(vector<vector<Vertex*> >::iterator oit = outlines.begin(); oit != outlines.end(); oit++){
        vector<Vertex*> outline = (vector<Vertex*>) *oit;
        vector<Vertex*>::iterator vit = outline.begin();
        Vertex* v1 = (Vertex*) *vit;
        Vertex* v2;
        vit++;

        for(; vit != outline.end(); vit++){
            v2 = (Vertex*) *vit;
            perimeter += ((*v1) - (*v2)).length();
            v1 = v2;
        }

        perimeter += ((*v2) - (**(outline.begin()))).length();
    }
}

